#ifndef _INDIGO_X11_UTIL_H
#define _INDIGO_X11_UTIL_H

#ifndef _INDIGO_COLOR_H
#include <indigo/Color.h>
#endif

#ifndef _XLIB_H_
#include <X11/Xlib.h>
#endif

#ifndef _XtIntrinsic_h
#include <X11/Intrinsic.h>
#endif

#ifndef _TIMBER_MEDIA_IMAGEBUFFER_H
#include <timber/media/ImageBuffer.h>
#endif

namespace indigo {
   namespace x11 {
      class X11Image;

      /** This environment variable controls the use of the XRender extension; unless set to false, XRender will be used.
       * The name of this variable is <tt>INDIGO_X11_RENDERER</tt>.
       */
      extern const char* INDIGO_X11_XRENDERER;

      /**
       * Create a pixmap in a safe way. This method ensures that the pixmap will have been created by
       * synchronizing the X11 connection.
       * @param dpy the display
       * @param drawable a drawable
       * @param width the pixmap's width
       * @param height the pixmap's height
       * @param depth the pixmap's depth
       * @return a pixmap or 0 if not defined
       */
      Pixmap createPixmap(Display *dpy, Drawable drawable, ::std::uint32_t width, ::std::uint32_t height,
            ::std::uint32_t depth)
      throws();

      /**
       * Determine if the XRender extension is available for the specified display.
       * @param display
       * @return true if the XRender extension is available.
       */
      bool isXRenderEnabled (Display* display) throws();

      /**
       * Log the set of available X extension to the logger.
       * @param display
       */
      void logXExtensions(Display* display)
      throws();

      /**
       * Map a color to a pixel. The opacity is ignored.
       * @param display a display
       * @param colors a colormap
       * @param c a color
       * @return a pixel for the color
       */
      inline Pixel convertColorToPixel(Display* display, Colormap colors, const Color& c)
      throws()
      {
         XColor color;
         c.rgb16a(color.red,color.green,color.blue);
         if (::XAllocColor(display,colors,&color)) {
            return color.pixel;
         }
         else {
            return 0;
         }
      }

      /**
       * Create an X11image from an externally specified image.
       * @param src the source image
       * @param dpy the display
       * @param vis a visual
       * @param depth the image depth to be used
       * @param imagePtr an existing image pointer (can be 0)
       * @return a new X11 image
       */
      ::std::unique_ptr< X11Image> createX11Image(const ::timber::media::ImageBuffer& src, Display* dpy, Visual* vis,
            size_t depth, ::std::unique_ptr< X11Image> imagePtr)
      throws();

      /**
       * Create an X11image from an externally specified image.
       * @param src the source image
       * @param dpy the display
       * @param imagePtr an existing image pointer (can be 0)
       * @return a new X11 image
       */
      ::std::unique_ptr< X11Image> createX11Image(const ::timber::media::ImageBuffer& src, Display* dpy,
            ::std::unique_ptr< X11Image> imagePtr)
throws   ();

}
}

#endif
