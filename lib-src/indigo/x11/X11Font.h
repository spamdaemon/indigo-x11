#ifndef _INDIGO_X11_X11FONT_H
#define _INDIGO_X11_X11FONT_H

#ifndef _INDIGO_FONT_H
#include <indigo/Font.h>
#endif

#include <X11/Xlib.h>

namespace indigo {
  namespace x11 {
    
    /**
     * This abstract class extends the font ID interface with
     * a method to obtain an X11 font.
     */
    class X11Font : public ::indigo::Font {
      X11Font(const X11Font&);
      X11Font&operator=(const X11Font&);

      /** Default constructor. */
    protected:
      X11Font() throws();
	

      /** The destructor */
    public:
      ~X11Font() throws();

      /**
       * Get the X11 font implementation.
       * @return the X11 font implementation
       */
    public:
      virtual ::Font x11Font() const throws() = 0;
    };
      
  }
}

#endif
