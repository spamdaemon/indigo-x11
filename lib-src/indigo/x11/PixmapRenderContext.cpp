#include <indigo/x11/PixmapRenderContext.h>
#include <indigo/x11/util.h>

using namespace ::timber;

namespace indigo {
   namespace x11 {
      namespace {

         static Pixmap newPixmap(const X11RenderContext& context, ::std::uint32_t w, ::std::uint32_t h,
               ::std::uint32_t depth)
throws      (::std::exception)
      {
         Pixmap pm = createPixmap(context.display(), context.drawable(), w, h, depth);
         if (pm==0) {
            throw ::std::runtime_error("Could not allocate pixmap");
         }
         return pm;
      }
   }

   PixmapRenderContext::PixmapRenderContext(const ::timber::Reference< X11RenderContext>& ctx, ::std::uint32_t w,
         ::std::uint32_t h, ::std::uint32_t d)
   throws (::std::exception)
   :
   X11RenderContext(ctx->display(),ctx->gc(),newPixmap(*ctx,w,h,d),ctx->colors()),_context(ctx),
   _width(w), _height(h),_depth(d)
   {}

   PixmapRenderContext::~PixmapRenderContext()
   throws()
   {
      Pixmap pm = drawable();
      XFreePixmap(display(),pm);
   }

}
}
