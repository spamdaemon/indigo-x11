#ifndef _INDIGO_X11_PIXMAPRENDERCONTEXT_H
#define _INDIGO_X11_PIXMAPRENDERCONTEXT_H

#ifndef _INDIGO_X11_X11RENDERCONTEXT_H
#include <indigo/x11/X11RenderContext.h>
#endif

#ifndef _TIMBER_LOGGING_H
#include <timber/logging.h>
#endif

namespace indigo {
   namespace x11 {

      /**
       * This class is a render context for rendering an offset image onto a pixmap.
       */
      class PixmapRenderContext : public X11RenderContext
      {
            PixmapRenderContext(const PixmapRenderContext&);
            PixmapRenderContext&operator=(const PixmapRenderContext&);

            /**
             * Create a renderer based on the specified context.
             * @param ctx an original context
             * @param w the pixmap's width
             * @param h the pixmap's height
             */
         public:
            PixmapRenderContext(const ::timber::Reference< X11RenderContext>& ctx, ::std::uint32_t w,
                  ::std::uint32_t h, ::std::uint32_t d)
            throws ( ::std::exception);

            /** Destroy this renderer. */
         public:
            ~PixmapRenderContext()throws();

            /**
             * Get the pixmap. Do not destroy the pixmap.
             * @return the pixmap created by this renderer
             */
         public:
            inline Pixmap pixmap() const throws()
            {  return static_cast<Pixmap>(drawable());}

            /**
             * Get the width of the pixmap.
             * @return pixmap's width
             */
         public:
            inline ::std::uint32_t width() const throws() {return _width;}

            /**
             * Get the height of the pixmap.
             * @return pixmap's height
             */
         public:
            inline ::std::uint32_t height() const throws() {return _height;}

            /**
             * Get the depth of the pixmap.
             * @return pixmap's deoth
             */
         public:
            inline ::std::uint32_t depth() const throws() {return _depth;}

            /** The original context */
         private:
            const ::timber::Reference< X11RenderContext> _context;

            /** The width of the pixmap */
         private:
            ::std::uint32_t _width;

            /** The height of the pixmap */
         private:
            ::std::uint32_t _height;

            /** The depth of the pixmap */
         private:
            std::uint32_t _depth;

      };
   }
}

#endif
