#include <indigo/x11/SimpleRenderer.h>
#include <indigo/x11/X11RenderContext.h>
#include <indigo/x11/X11Font.h>
#include <indigo/x11/X11Image.h>
#include <indigo/x11/util.h>
#include <indigo/Color.h>
#include <indigo/Point.h>
#include <indigo/AffineTransform2D.h>
#include <indigo/indigo.h>

#include <iostream>

using namespace ::std;
using namespace ::timber;

namespace indigo {
   namespace x11 {

      SimpleRenderer::Pen::Pen(const ::timber::Reference< X11RenderContext>& ctx)
      throws()
            : _ctx(ctx), _changed(true), _pixel(0), _alpha(1), _color(Color::BLACK)
      {
      }
      SimpleRenderer::Pen::~Pen()
      throws()
      {
      }

      void SimpleRenderer::Pen::setAlpha(double a)
      throws()
      {
         if (_alpha != a) {
            _changed = true;
            _alpha = a;
         }
      }

      void SimpleRenderer::Pen::setColor(const Color& c)
      throws()
      {
         if (_color != c) {
            _color = c;
            _changed = true;
         }
      }

      ::Pixel SimpleRenderer::Pen::getPixel() const throws()
      {
         if (_changed) {
            _changed = false;
            const Color c = _color.getPremultipliedColor(_alpha);
            _pixel = convertColorToPixel(_ctx->display(), _ctx->colors(), c);
         }
         return _pixel;
      }

      SimpleRenderer::SimpleRenderer(const ::timber::Reference< X11RenderContext>& ctx)
      throws()
            : _ctx(ctx), _fill(ctx), _stroke(ctx), _fontColor(ctx), _alpha(1)
      {
         _pixel = _fill.getPixel();
         XSetForeground(ctx->display(), ctx->gc(), _pixel);
      }

      SimpleRenderer::~SimpleRenderer()
      throws()
      {
      }

      void SimpleRenderer::setFillColor(const Color& c)
      throws()
      {
         _fill.setColor(c);
      }

      void SimpleRenderer::setStrokeColor(const Color& c)
      throws()
      {
         _stroke.setColor(c);
         _fontColor.setColor(c);
      }

      void SimpleRenderer::setStrokeWidth(double w)
      throws()
      {
      }

      void SimpleRenderer::setAntiAliasingMode(AntiAliasingMode mode)
      throws()
      {
         // Anti-aliasing is not supported
      }

      void SimpleRenderer::setAlpha(double a)
      throws()
      {
         _alpha = a;
         _fill.setAlpha(a);
         _stroke.setAlpha(a);
         _fontColor.setAlpha(a);
      }

      void SimpleRenderer::activatePen(const Pen& p)
      throws()
      {
         ::Pixel col = p.getPixel();
         if (col != _pixel) {
            _pixel = col;
            XSetForeground(_ctx->display(), _ctx->gc(), _pixel);
         }
      }

      SimpleRenderer::OutPoint SimpleRenderer::offsetPoint(const OutPoint& pt, ::timber::Short offsetX,
            ::timber::Short offsetY) const throws()
      {
         OutPoint xp;
         xp.x = pt.x + offsetX;
         xp.y = pt.y + offsetY;
         return xp;
      }

      SimpleRenderer::OutPoint SimpleRenderer::makePoint(const Point& pt) const throws()
      {
         OutPoint xp;
         makePoint(pt, xp);
         return xp;
      }

      void SimpleRenderer::makePoint(const Point& in, OutPoint& out) const throws()
      {
         Int x = roundToInt(in.x());
         Int y = roundToInt(in.y());
         if (x < -32768) {
            x = -32768;
         }
         else if (x >= 32768) {
            x = 32767;
         }
         if (y < -32768) {
            y = -32768;
         }
         else if (y >= 32768) {
            y = 32767;
         }

         out.x = static_cast< Short>(x);
         out.y = static_cast< Short>(y);
      }

      SimpleRenderer::OutPoint SimpleRenderer::makePointSafe(const Point& pt) const throws()
      {
         OutPoint xp;
         makePointSafe(pt, xp);
         return xp;
      }

      void SimpleRenderer::makePointSafe(const Point& in, OutPoint& out) const throws()
      {
         Int x = roundToInt(in.x());
         Int y = roundToInt(in.y());
         out.x = static_cast< Short>(x);
         out.y = static_cast< Short>(y);
      }

      void SimpleRenderer::fillTriangles(const OutPoint* pts, size_t n)
      throws()
      {
         activatePen(_fill);

         for (const OutPoint* end = pts + n; pts < end; pts += 3) {
            ::XFillPolygon(_ctx->display(), _ctx->drawable(), _ctx->gc(), const_cast< OutPoint*>(pts), 3, Convex,
                  CoordModeOrigin);
         }
      }

      void SimpleRenderer::fillTriStrip(OutPoint* pts, size_t n)
      throws()
      {
         activatePen(_fill);
         OutPoint* end = pts + n - 2;

         while (pts < end) {
            ::XFillPolygon(_ctx->display(), _ctx->drawable(), _ctx->gc(), pts, 3, Convex, CoordModeOrigin);
            ++pts;
         }
      }

      void SimpleRenderer::fillTriFan(OutPoint* pts, size_t n)
      throws()
      {
         activatePen(_fill);
         OutPoint* end = pts + n - 2;
         while (pts < end) {
            ::XFillPolygon(_ctx->display(), _ctx->drawable(), _ctx->gc(), pts, 3, Convex, CoordModeOrigin);
            *(pts + 1) = *pts;
            ++pts;
         }
      }

      void SimpleRenderer::fillConvexPolygon(const OutPoint* pts, size_t n)
      throws()
      {
         activatePen(_fill);
         ::XFillPolygon(_ctx->display(), _ctx->drawable(), _ctx->gc(), const_cast< OutPoint*>(pts), n, Convex,
               CoordModeOrigin);
      }

      void SimpleRenderer::fillPolygon(OutPoint* pts, size_t n)
      throws()
      {
         activatePen(_fill);
         ::XFillPolygon(_ctx->display(), _ctx->drawable(), _ctx->gc(), const_cast< OutPoint*>(pts), n, Nonconvex,
               CoordModeOrigin);
      }

      void SimpleRenderer::drawPolygon(const OutPoint* pts, size_t n)
      throws()
      {
         activatePen(_stroke);
         ::XDrawLines(_ctx->display(), _ctx->drawable(), _ctx->gc(), const_cast< OutPoint*>(pts), n, CoordModeOrigin);
      }

      void SimpleRenderer::fillRectangle(const OutPoint& tl, const OutPoint& br)
      throws()
      {
         activatePen(_fill);
         ::XFillRectangle(_ctx->display(), _ctx->drawable(), _ctx->gc(), tl.x, tl.y, br.x - tl.x + 1, br.y - tl.y + 1);
      }

      void SimpleRenderer::drawRectangle(const OutPoint& tl, const OutPoint& br)
      throws()
      {
         activatePen(_stroke);
         ::XDrawRectangle(_ctx->display(), _ctx->drawable(), _ctx->gc(), tl.x, tl.y, br.x - tl.x + 1, br.y - tl.y + 1);
      }

      void SimpleRenderer::drawPoints(const OutPoint* pts, size_t n)
      throws()
      {
         activatePen(_stroke);
         const OutPoint* pt = pts;
         const OutPoint* end = pts + n;
         while (pt != end) {
            ::XDrawPoint(_ctx->display(), _ctx->drawable(), _ctx->gc(), pt->x, pt->y);
            ++pt;
         }
      }

      void SimpleRenderer::drawPolyline(const OutPoint* pts, size_t n, bool close)
      throws()
      {
         activatePen(_stroke);
         ::XDrawLines(_ctx->display(), _ctx->drawable(), _ctx->gc(), const_cast< OutPoint*>(pts), n, CoordModeOrigin);
         if (close) {
            OutPoint xpts[2] = { pts[0], pts[n - 1] };
            ::XDrawLines(_ctx->display(), _ctx->drawable(), _ctx->gc(), const_cast< OutPoint*>(xpts), 2,
                  CoordModeOrigin);
         }
      }

      void SimpleRenderer::renderImage(const ::timber::media::ImageBuffer& image, const OutPoint& topLeft)
      throws()
      {
         if (_alpha == 0) {
            return;
         }

         if (_alpha != 1) {
            //FIXME: premultiply image by alpha
         }

         _image = createX11Image(image, _ctx->display(), move(_image));
         if (_image.get() == 0) {
            return;
         }

         ::XPutImage(_ctx->display(), _ctx->drawable(), _ctx->gc(), _image->x11Image(), // image
               0, 0, // src pos
               topLeft.x, topLeft.y, // dest pos
               _image->width(), _image->height());
      }

      void SimpleRenderer::renderImage(const ::timber::media::ImageBuffer& image, const AffineTransform2D& T)
      throws()
      {
         const Point pt(T(0, 2), T(1, 2));
         const OutPoint topLeft = makePoint(pt);
         renderImage(image, topLeft);
      }

      void SimpleRenderer::setFont(const ::timber::Pointer< ::indigo::Font>& font)
      {
         Pointer< X11Font> f = font.tryDynamicCast< X11Font>();
         _font = f;
         if (f) {
            ::XSetFont(_ctx->display(), _ctx->gc(), f->x11Font());
         }
      }

      void SimpleRenderer::drawText(const ::std::string& str, const OutPoint& o)
      {
         if (_font) {
            activatePen(_fontColor);
            ::XDrawString(_ctx->display(), _ctx->drawable(), _ctx->gc(), o.x, o.y, str.c_str(), str.length());
         }
      }

      Color SimpleRenderer::readPixel(const OutPoint& pt)
      {
         XColor col;
         ::XFlush(_ctx->display());
         XImage* ximage = XGetImage(_ctx->display(), _ctx->drawable(), pt.x, pt.y, 1, 1, -1, ZPixmap);
         col.pixel = XGetPixel(ximage, 0, 0);
         XDestroyImage(ximage);

         XQueryColor(_ctx->display(), _ctx->colors(), &col);

         return Color(col.red / (double) 0xffff, col.green / (double) 0xffff, col.blue / (double) 0xffff);
      }

      void SimpleRenderer::setClipRectangle(const OutPoint& ul, const OutPoint& lr)
      throws ()
      {
         ::XRectangle rect;
         rect.x = ul.x;
         rect.y = ul.y;
         rect.width = 1 + lr.x - ul.x;
         rect.height = 1 + lr.y - ul.y;

         ::XSetClipRectangles(_ctx->display(), _ctx->gc(), 0, 0, &rect, 1, Unsorted);
      }
   }
}
