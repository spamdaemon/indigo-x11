#ifndef _INDIGO_X11_XRENDERER_H
#define _INDIGO_X11_XRENDERER_H

#ifndef _XRENDER_H
#include <X11/extensions/Xrender.h>
#endif

#ifndef _XLIB_H_
#include <X11/Xlib.h>
#endif

#ifndef X_H
#include <X11/X.h>
#endif

#ifndef _XtIntrinsic_h
#include <X11/Intrinsic.h>
#endif

#ifndef _TIMBER_H_
#include <timber/timber.h>
#endif

#ifndef _TIMBER_MEDIA_IMAGEBUFFER_H
#include <timber/media/ImageBuffer.h>
#endif

#ifndef _INDIGO_X11_SIMPLERENDERER_H
#include <indigo/x11/SimpleRenderer.h>
#endif

#include <X11/Xft/Xft.h>

#include <GL/glu.h>

#include <vector>

#ifndef _INDIGO_ANTIALIASINGMODE_H
#include <indigo/AntiAliasingMode.h>
#endif

namespace indigo {
   class Color;
   class Point;
   class Font;
   class AffineTransform2D;

   namespace x11 {
      class XRenderFont;
      class X11Image;
      class X11RenderContext;

      class XRenderer
      {

            /** The output point type */
         public:
            typedef ::XPointFixed OutPoint;

            /** A brush is used to fill closed areas */
         private:
            struct Brush
            {
                  enum ChangeFlag
                  {
                     ALPHA = 1, COLOR = 2, AAMODE = 4, ALL = 0xff
                  };

                  enum Visibility
                  {
                     INVISIBLE, TRANSLUCENT, OPAQUE
                  };

                  /** Default constructor */
                  Brush(::Display* dpy)throws();

                  ~Brush()throws();

                  /**
                   * Get the visibility of this brush
                   */
               public:
                  Visibility visibility() const throws();

                  /**
                   * Get the picture for this brush.
                   * @return the picture
                   */
               public:
                  Picture operator()() const throws();

                  /**
                   * Set the alpha for this brush
                   * @param alpha the new alpha value
                   */
               public:
                  void setAlpha(double alpha)throws();

                  /**
                   * Set the solid fill color
                   * @param c the fill color
                   */
               public:
                  void setColor(const Color& c)throws();

                  /**
                   * Set the anti-aliasing mode.
                   * @param mode the new anti-aliasing mode
                   */
               public:
                  void setAAMode(AntiAliasingMode m)throws();

                  /**
                   * Get the aamode
                   * @return aa mode
                   */
               public:
                  inline AntiAliasingMode aaMode() const throws() {return _aaMode;}

                  /** The display */
               private:
                  ::Display* _display;

                  /** The change flag (0 = no change) */
               private:
                  mutable ::canopy::UInt8 _changeFlags;

                  /** The current alpha */
               private:
                  double _alpha;

                  /** The current fill color */
               private:
                  Color _color;

                  /** The anti-aliasing mode */
               private:
                  AntiAliasingMode _aaMode;

                  /**
                   * The color format; this is a ARGB format enabling translucency; it's the source format
                   * because the shapes to be rendered are the source and the X11 window is the destination
                   */
               private:
                  XRenderPictFormat* _format;

                  /** The source pixmap needed to fill the paint brush */
               private:
                  Pixmap _drawable;

                  /** The source picture filled with the current color */
               private:
                  Picture _pict;
            };

            /** A pen is used to render 1-dimensional objects */
         private:
            struct Pen
            {
                  enum ChangeFlag
                  {
                     ALPHA = 1, COLOR = 2, WIDTH = 4, AAMODE = 8, ALL = 0xff
                  };
                  enum Visibility
                  {
                     INVISIBLE, TRANSLUCENT, OPAQUE
                  };

                  /** Default constructor */
                  Pen(::Display* dpy)throws();

                  ~Pen()throws();

                  /**
                   * Get the visibility of this brush
                   */
               public:
                  Visibility visibility() const throws();

                  /**
                   * Get the picture for this brush.
                   * @return the picture
                   */
               public:
                  Picture operator()() const throws();

                  /**
                   * Get the width of this pen.
                   * @return the width of this pen
                   */
               public:
                  inline double width() const throws() {return _width;}

                  /**
                   * Set the alpha for this brush
                   * @param alpha the new alpha value
                   */
               public:
                  void setAlpha(double alpha)throws();

                  /**
                   * Set the solid fill color
                   * @param c the fill color
                   */
               public:
                  void setColor(const Color& c)throws();

                  /**
                   * Set the anti-aliasing mode.
                   * @param mode the new anti-aliasing mode
                   */
               public:
                  void setAAMode(AntiAliasingMode m)throws();

                  /**
                   * Get the aamode
                   * @return aa mode
                   */
               public:
                  inline AntiAliasingMode aaMode() const throws() {return _aaMode;}
                  /**
                   * Set the solid fill color
                   * @param c the fill color
                   */
               public:
                  void setWidth(double w)throws();

                  /** The display */
               private:
                  ::Display* _display;

                  /** The change flag (0 = no change) */
               private:
                  mutable ::canopy::UInt8 _changeFlags;

                  /** The current alpha */
               private:
                  double _alpha;

                  /** The width */
               private:
                  double _width;

                  /** The current fill color */
               private:
                  Color _color;

                  /** The anti-aliasing mode */
               private:
                  AntiAliasingMode _aaMode;

                  /**
                   * The color format; this is a ARGB format enabling translucency; it's the source format
                   * because the shapes to be rendered are the source and the X11 window is the destination
                   */
               private:
                  XRenderPictFormat* _format;

                  /** The source pixmap needed to fill the paint brush */
               private:
                  Pixmap _drawable;

                  /** The source picture filled with the current color */
               private:
                  Picture _pict;
            };

            /** A pen is used to render 1-dimensional objects */
         private:
            struct FontPen
            {
                  enum ChangeFlag
                  {
                     ALPHA = 1, COLOR = 2, FONT = 4, AAMODE = 8, ALL = 0xff
                  };
                  enum Visibility
                  {
                     INVISIBLE, TRANSLUCENT, OPAQUE
                  };

                  /** Default constructor */
                  FontPen(::Display* dpy, ::Drawable c, ::Colormap colormap)throws();

                  ~FontPen()throws();

                  /**
                   * Draw a string with the current font
                   */
                  void drawText(const ::std::string& str, const OutPoint& o);

                  /**
                   * Get the visibility of this brush
                   */
               public:
                  Visibility visibility() const throws();

                  /**
                   * Get the picture for this brush.
                   * @return the picture
                   */
               public:
                  XftDraw* operator()() const throws();

                  /**
                   * Set the anti-aliasing mode.
                   * @param mode the new anti-aliasing mode
                   */
               public:
                  void setAAMode(::indigo::AntiAliasingMode m)throws();

                  /**
                   * Get the aamode
                   * @return aa mode
                   */
               public:
                  inline AntiAliasingMode aaMode() const throws() {return _aaMode;}

                  /**
                   * Set the alpha for this brush
                   * @param alpha the new alpha value
                   */
               public:
                  void setAlpha(double alpha)throws();

                  /**
                   * Set the solid fill color
                   * @param c the fill color
                   */
               public:
                  void setColor(const Color& c)throws();

                  /**
                   * Get the actual font color. This value may not be cached.
                   * @return the font color
                   */
               public:
                  XftColor* fontColor() const throws();

                  /**
                   * Get the current font
                   * @return the current font
                   */
               public:
                  inline const ::timber::Pointer< XRenderFont>& font() const throws()
                  {  return _font;}

                  /**
                   * Set the current font
                   * @param f the new font
                   */
               public:
                  void setFont(const ::timber::Pointer< ::indigo::x11::XRenderFont>& f)throws();

                  /** The display */
               private:
                  ::Display* _display;

                  /** The change flag (0 = no change) */
               private:
                  mutable ::canopy::UInt8 _changeFlags;

                  /** The anti-aliasing mode */
               private:
                  AntiAliasingMode _aaMode;

                  /** The current alpha */
               private:
                  double _alpha;

                  /** The current fill color */
               private:
                  Color _color;

                  /** A colormap */
               private:
                  mutable ::Colormap _colors;

                  /** The XFT drawable needed for font rendering */
               private:
                  XftDraw* _fontContext;

                  /** The current xft color */
               private:
                  mutable XftColor _fontColor;

                  /** The current font */
               private:
                  ::timber::Pointer< XRenderFont> _font;
            };

            /**
             * An alpha mask.
             */
         private:
            struct Alpha
            {
                  enum ChangeFlag
                  {
                     ALPHA = 1, ALL = 0xff
                  };
                  enum Visibility
                  {
                     INVISIBLE, TRANSLUCENT, OPAQUE
                  };

                  /** Create an alpha mask */
                  Alpha(::Display* dpy, size_t w, size_t h, AntiAliasingMode aaMode)throws();

                  ~Alpha()throws();

                  /**
                   * Get the visibility of this brush
                   */
               public:
                  Visibility visibility() const throws();

                  /**
                   * Clear the mask.
                   */
               public:
                  void clear()throws();

                  /**
                   * Get the picture for this brush.
                   * @return the picture
                   */
               public:
                  Picture operator()() const throws();

                  /**
                   * Set the alpha for this brush
                   * @param alpha the new alpha value
                   */
               public:
                  void setAlpha(double alpha)throws();

                  /** The display */
               private:
                  ::Display* _display;

                  /** The change flag (0 = no change) */
               private:
                  mutable ::canopy::UInt8 _changeFlags;

                  /** The current alpha */
               private:
                  double _alpha;

                  /** An simple alpha map for use with add Traps */
               private:
                  XRenderPictFormat* _format;

                  /** The source pixmap needed to fill the paint brush */
               private:
                  Pixmap _drawable;

                  /** The source picture filled with the current color */
               private:
                  Picture _pict;

                  /** The width and height of this mask */
               private:
                  size_t _width, _height;
            };

            /** A GL vertex */
         private:
            struct GLPoint
            {
                  GLdouble vertex[3];
            };

            /** The tesselation data */
         public:
            struct GLUTessData
            {

                  /** The tesselator */
                  GLUtesselator* _tesselator;

                  /** The output points */
                  ::std::vector< OutPoint> _triPoints;

                  /** The GL doubles */
                  ::std::vector< GLPoint> _glPoints;

                  /** The current type */
                  GLenum _type;
            };

            /**
             * Create a new X11Render policy.
             * @param dp the display
             * @param gc the graphic context
             * @param canvas the drawable
             * @param colormap a color map
             * @param w the width of the canvas
             * @param h the height of the canvas
             */
         public:
            XRenderer(const ::timber::Reference<X11RenderContext>& ctx, size_t w, size_t h)throws();

            /** Destructor */
         public:
            ~XRenderer()throws();

            /**
             * Set a clip rectangle.
             * @param ul the upper left coordinate
             * @param lr the lower right coordinate
             */
         public:
            void setClipRectangle(const OutPoint& ul, const OutPoint& lr)throws();

            /**
             * Set the solid fill color. This replaces any previously set fill pattern
             * or color.
             * @param c a fill color
             */
            void setFillColor(const Color& c)throws();

            /**
             * Set the stroke color.
             * @param c a color
             */
            void setStrokeColor(const Color& c)throws();

            /**
             * Set the stroke width.
             * @param w the stroke width
             */
            void setStrokeWidth(double w)throws();

            /**
             * Set the alpha value
             * @param alpha the new alpha value
             */
            void setAlpha(double a)throws();

            /**
             * Set the anti-aliasing mode for filling polygons.
             * @param mode the anti-aliasing mode
             */
            void setAntiAliasingMode(AntiAliasingMode mode)throws();

            /**
             * Offset the specified point.
             * @param pt a point
             * @param offsetX the offset in x
             * @param offsetY the offset in y
             * @return a new point that is offset the specified number of pixels
             */
            OutPoint offsetPoint(const OutPoint& pt, ::timber::Short offsetX, ::timber::Short offsetY) const throws();

            /**
             * Make a point.
             * @param pt
             */
            OutPoint makePoint(const Point& pt) const throws();

            /**
             * A make a point.
             * @param in the input point
             * @param out the output point
             */
            void makePoint(const Point& in, OutPoint& out) const throws();

            /**
             * Make a point.
             * @param pt
             */
            OutPoint makePointSafe(const Point& pt) const throws();

            /**
             * A make a point.
             * @param in the input point
             * @param out the output point
             */
            void makePointSafe(const Point& in, OutPoint& out) const throws();

            /**
             * Fill a polygon. The polygon is assumed to be non-convex.
             * @param pts the polygon points
             * @param n the number of points
             */
            void fillPolygon(OutPoint* pts, size_t n)throws();

            /**
             * Draw the outline of a polygon. This can be simulated with drawPolyline
             * to some extent, but the scanline conversion for a polygon is typically
             * different from that of poly line.
             * @param pts the polygon points
             * @param n the number of points
             */
            void drawPolygon(const OutPoint* pts, size_t n)throws();

            /**
             * Fill a polygon. The polygon is assumed to be convex.
             * @param pts the polygon points
             * @param n the number of points
             */
            void fillConvexPolygon(OutPoint* pts, size_t n)throws();

            /**
             * Fill a list of triangles. The number of points provided
             * must be 3*n.
             * @param pts the triangle vertices
             * @param n the number of points.
             */
            void fillTriangles(const OutPoint* pts, size_t n)throws();

            /**
             * Fill a list of triangles defined as a triangle strip. The input points
             * are modified by this call.
             * @param pts the triangle vertices
             * @param n the number of points defining the triangle strip
             */
            void fillTriStrip(OutPoint* pts, size_t n)throws();

            /**
             * Fill a list of triangles defined as a triangle fan. The input points
             * are modified by this call.
             * @param pts the triangle vertices
             * @param n the number of points defining the triangle fan
             */
            void fillTriFan(OutPoint* pts, size_t n)throws();

            /**
             * Draw a filled rectangle defined by its top-left  and lower-right corners. The points
             * are inclusive!
             * @param ul the top-left corner
             * @param lr the lower-right corner
             */
            void fillRectangle(const OutPoint& ul, const OutPoint& lr)throws();

            /**
             * Draw a points (1 pixel wide only)
             * @param pts the polygon points
             * @param n the number of points
             */
            void drawPoints(const OutPoint* pts, size_t n)throws();

            /**
             * Draw a polyline (1 pixel wide only)
             * @param pts the polygon points
             * @param n the number of points
             * @param true if true draw a line back to the first point
             */
            void drawPolyline(const OutPoint* pts, size_t n, bool close = false)throws();

            /**
             * Draw rectangle defined by its top-left  and lower-right corners. The points
             * are inclusive!
             * @param ul the top-left corner
             * @param lr the lower-right corner
             */
            void drawRectangle(const OutPoint& ul, const OutPoint& lr)throws();

            /**
             * Render an image. The location of the image is defined by the affine transform.
             * @param image an image
             * @param T a translation of the image
             */
            void renderImage(const ::timber::media::ImageBuffer& image, const OutPoint& T)throws();

            /**
             * Render an image. The location of the image is defined by the affine transform.
             * @param image an image
             * @param  an affine transform that as applied to the image.
             */
            void renderImage(const ::timber::media::ImageBuffer& image, const AffineTransform2D& T)throws();

            /**
             * Set the current font
             * @param f the new font
             */
            void setFont(const ::timber::Pointer< ::indigo::Font>& font);

            /**
             * Draw a string with the current font
             */
            void drawText(const ::std::string& str, const OutPoint& o);

            /**
             * Read the color of the specified pixel.
             * @param pt a pixel
             * @return a color
             */
         public:
            Color readPixel(const OutPoint& pt);

            /**
             * Check a specific pixel. This is required by X11Renderer for picking.
             * @return always false
             */
         public:
            inline bool checkPixel()throws() {return false;}

            /** Glu tesselation */
         private:
            static void addVertex(GLdouble*, XRenderer*);
            static void beginObject(GLenum, XRenderer*);
            static void endObject(XRenderer*);

            /** The width and height of the canvas */
         private:
            const size_t _width, _height;

            /** The render context */
         private:
            ::timber::Reference<X11RenderContext> _ctx;

            /** XPoints */
         private:
            ::std::vector< ::XPoint> _xpoints;

            /** A vector of triangles */
         private:
            ::std::vector< XTriangle> _triangles;

            /** The format for the X11 window; this is the destination */
         private:
            XRenderPictFormat* _destFormat;

            /** The destination picture */
         private:
            Picture _dest;

            /** The size of the image drwable */
         private:
            size_t _imageWidth, _imageHeight;

            /** An image pixmap; this is used for compositing images */
         private:
            Pixmap _imageDrawable;

            /** The GC for the image */
         private:
            ::GC _imageGC;

            /** The image picture */
         private:
            Picture _image;

            /** The simple renderer which we'll use for now to simulate some shapes
             * that are hard to render
             */
         private:
            SimpleRenderer _legacy;

            /** The glu tesselation data */
         private:
            GLUTessData _gluTess;

            /** An X11 image */
         private:
            ::std::unique_ptr< X11Image> _ximage;

            /** The brush */
         private:
            Brush _brush;

            /** The pen */
         private:
            Pen _pen;

            /** A font pen */
         private:
            FontPen _fontPen;

            /** An alpha mask */
         private:
            Alpha _mask1x1;

            /** A normal alpha mask, the size of the window and supporting anti-aliasing */
         private:
            Alpha _mask;

            /** A normal alpha mask, not supporting anti-aliasing */
         private:
            Alpha _maskAANone;
      };
   }
}

#endif
