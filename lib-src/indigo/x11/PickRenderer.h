#ifndef _INDIGO_X11_PICKRENDERER_H
#define _INDIGO_X11_PICKRENDERER_H

#ifndef _TIMBER_H_
#include <timber/timber.h>
#endif

#ifndef _TIMBER_MEDIA_IMAGEBUFFER_H
#include <timber/media/ImageBuffer.h>
#endif

#ifndef _INDIGO_FONT_H
#include <indigo/Font.h>
#endif

#ifndef _INDIGO_NODES_H
#include <indigo/nodes.h>
#endif

#include <cassert>

namespace indigo {
   class Color;
   class Point;
   class Font;
   class AffineTransform2D;

   namespace x11 {
      class X11Font;
      class X11Image;

      template<class RENDERER> class PickRenderer
      {

            /** The output point type */
         public:
            typedef typename RENDERER::OutPoint OutPoint;

            /**
             *
             * @param impl the real renderer
             * @param pt the point to be observed
             */
         public:
            PickRenderer(::std::unique_ptr<RENDERER> impl, OutPoint& pt)throws()
            : _impl(move(impl)),_original(_impl->readPixel(pt)),_fg(0),
            _pixel(pt),_pixelChanged(false)
            {
               _bg = _original;
               Color inv = invertColor(_bg);
               setFillColor(inv);
               setStrokeColor(inv);
            }

            /** Destructor */
         public:
            ~PickRenderer()throws()
            {
               // clear the pixel
               _impl->setStrokeColor(_original);
               _impl->drawPoints(&_pixel,1);
            }

            /** Invert the specified color */
         private:
            inline static Color invertColor(const Color& c)
            {
               return Color(1.0 - c.red(), 1.0 - c.green(), 1.0 - c.blue());
            }

            /**
             * Read the color of the specified pixel.
             * @param pt a pixel
             * @return a color
             */
         public:
            Color readPixel(const OutPoint& pt)
            {
               return _impl->readPixel(pt);
            }

            /**
             * Check the current pixel. This may introduce side-effects!
             */
         private:
            bool checkPixelChanged(const Color& bg)throws()
            {
               if (_pixelChanged) {
                  return true;
               }
               const Color c = _impl->readPixel(_pixel);
               _pixelChanged = c.red() != bg.red() || c.green() != bg.green() || c.blue() != bg.blue();
               return _pixelChanged;
            }

            /**
             * Check the current pixel. This may introduce side-effects!
             */
         public:
            bool checkPixel()throws()
            {
               if (checkPixelChanged(_bg)) {
                  _pixelChanged = false;

                  // clear the pixel
                  _impl->setStrokeColor(_bg);
                  _impl->drawPoints(&_pixel,1);
                  // and reset the current stroke color
                  if (_fg) {
                     _impl->setStrokeColor(*_fg);
                  }
                  else {
                     _impl->setStrokeColor(invertColor(_bg));
                  }
                  return true;
               }
               else {
                  return false;
               }
            }

            /**
             * Set the alpha value
             * @param alpha the new alpha value
             */
            void setAlpha(double a)throws()
            {
               _impl->setAlpha(a);
            }

            /**
             * Set the anti-aliasing mode f.
             * @param mode the anti-aliasing mode
             */
            void setAntiAliasingMode(AntiAliasingMode mode)throws()
            {
               _impl->setAntiAliasingMode(mode);
            }

            /**
             * Set the solid fill color. This replaces any previously set fill pattern
             * or color.
             * @param c a fill color
             */
            void setFillColor(const Color& c)throws()
            {
               _fill = c;
               _fg = 0;
            }

            /**
             * Set the stroke color.
             * @param c a color
             */
            void setStrokeColor(const Color& c)throws()
            {
               _stroke = c;
               _fg = 0;
            }

            /**
             * Set the stroke width.
             * @param w the stroke width
             */
            void setStrokeWidth(double w)throws()
            {
               _impl->setStrokeWidth(w);
            }

            /**
             * Offset the specified point.
             * @param pt a point
             * @param offsetX the offset in x
             * @param offsetY the offset in y
             * @return a new point that is offset the specified number of pixels
             */
            OutPoint offsetPoint(const OutPoint& pt, ::timber::Short offsetX, ::timber::Short offsetY) const throws()
            {  return _impl->offsetPoint(pt,offsetX,offsetY);}

            /**
             * Make a point.
             * @param pt
             */
            OutPoint makePoint(const Point& pt) const throws()
            {  return _impl->makePoint(pt);}

            /**
             * A make a point.
             * @param in the input point
             * @param out the output point
             */
            void makePoint(const Point& in, OutPoint& out) const throws()
            {  _impl->makePoint(in,out);}

            /**
             * Make a point.
             * @param pt
             */
            OutPoint makePointSafe(const Point& pt) const throws()
            {  return _impl->makePointSafe(pt);}

            /**
             * A make a point.
             * @param in the input point
             * @param out the output point
             */
            void makePointSafe(const Point& in, OutPoint& out) const throws()
            {  _impl->makePointSafe(in,out);}

            /**
             * Fill a polygon. The polygon is assumed to be non-convex.
             * @param pts the polygon points
             * @param n the number of points
             */
            void fillPolygon(OutPoint* pts, size_t n)throws()
            {
               if (!activateFillColor()) {return;}
               _impl->fillPolygon(pts,n);
            }

            /**
             * Fill a polygon. The polygon is assumed to be convex.
             * @param pts the polygon points
             * @param n the number of points
             */
            void fillConvexPolygon(OutPoint* pts, size_t n)throws()
            {
               if (!activateFillColor()) {return;}
               _impl->fillConvexPolygon(pts,n);
            }

            /**
             * Fill a list of triangles. The number of points provided
             * must be 3*n.
             * @param pts the triangle vertices
             * @param n the number of triangles.
             */
            void fillTriangles(const OutPoint* pts, size_t n)throws()
            {
               if (!activateFillColor()) {return;}
               _impl->fillTriangles(pts,n);
            }

            /**
             * Fill a list of triangles defined as a triangle strip. The input points
             * are modified by this call.
             * @param pts the triangle vertices
             * @param n the number of points defining the triangle strip
             */
            void fillTriStrip(OutPoint* pts, size_t n)throws()
            {
               if (!activateFillColor()) {return;}
               _impl->fillTriStrip(pts,n);
            }

            /**
             * Fill a list of triangles defined as a triangle fan. The input points
             * are modified by this call.
             * @param pts the triangle vertices
             * @param n the number of points defining the triangle fan
             */
            void fillTriFan(OutPoint* pts, size_t n)throws()
            {
               if (!activateFillColor()) {return;}
               _impl->fillTriFan(pts,n);
            }

            /**
             * Draw a filled rectangle defined by its top-left  and lower-right corners. The points
             * are inclusive!
             * @param ul the top-left corner
             * @param lr the lower-right corner
             */
            void fillRectangle(const OutPoint& ul, const OutPoint& lr)throws()
            {
               if (!activateFillColor()) {return;}
               _impl->fillRectangle(ul,lr);
            }

            /**
             * Draw the outline of a polygon. This can be simulated with drawPolyline
             * to some extent, but the scanline conversion for a polygon is typically
             * different from that of poly line.
             * @param pts the polygon points
             * @param n the number of points
             */
            void drawPolygon(const OutPoint* pts, size_t n)throws()
            {
               if (!activateStrokeColor()) {return;}
               _impl->drawPolygon(pts,n);
            }

            /**
             * Draw a points (1 pixel wide only)
             * @param pts the polygon points
             * @param n the number of points
             */
            void drawPoints(const OutPoint* pts, size_t n)throws()
            {
               if (!activateStrokeColor()) {return;}
               _impl->drawPoints(pts,n);
            }

            /**
             * Draw a polyline (1 pixel wide only)
             * @param pts the polygon points
             * @param n the number of points
             */
            void drawPolyline(const OutPoint* pts, size_t n)throws()
            {
               if (!activateStrokeColor()) {return;}
               _impl->drawPolyline(pts,n);
            }

            /**
             * Draw rectangle defined by its top-left  and lower-right corners. The points
             * are inclusive!
             * @param ul the top-left corner
             * @param lr the lower-right corner
             */
            void drawRectangle(const OutPoint& ul, const OutPoint& lr)throws()
            {
               if (!activateStrokeColor()) {return;}
               _impl->drawRectangle(ul,lr);
            }

            void renderImage(const ::timber::media::ImageBuffer& image, const OutPoint& tl)throws()
            {
               if (_pixelChanged) {
                  return;
               }
               const AffineTransform2D t(0,1,tl.x,tl.y);
               renderImage(image,t);
            }

            /**
             * Render an image for picking. This is more complicated, because we don't know ahead of time
             * what color will be assigned to the pixel if the image overlaps the pixel. So, we render twice.
             * If the after the first rendering, checkPixel() would return true, then we don't render it again.
             * But if the image happens to have the same pixel color as the current background color, then we
             * can't tell; so, we change the background color and paint the image again. This time, if checkPixel()
             * returns true, we paint the pixel in original foreground color and reset the foreground and background
             * colors, so that the next call to checkPixel() will indeed return true; otherwise we clear the pixel
             * and reset the colors, so that the next call to checkPixel() returns false.
             * @param image an image
             * @param topleft the top-left corner of the image on the screen.
             */
            void renderImage(const ::timber::media::ImageBuffer& image, const AffineTransform2D& T)throws()
            {
               if (_pixelChanged) {
                  return;
               }
               // for picking we need to do something rather special; we need to render the image once, then we check
               // the color; if we can tell that it is different, we're all set, but if not we need to change the
               // foreground color and repaint the image

               const Color c = _impl->readPixel(_pixel);
               _impl->renderImage(image,T);
               if (checkPixelChanged(c)) {
                  return;
               }

               // ok, the pixel is equal to the background; there are two reasons for this:
               // 1. the image actually has a pixel with the same value as the current pixel
               // 2. the image is translucent at the pixel to precision that it's actually invisible
               // at any rate, since we know that the background color is what will be drawn, we
               // color the pixel in the foreground color
               Color fg = invertColor(c);
               _impl->setStrokeColor(fg);
               _impl->drawPoints(&_pixel,1);

               _impl->renderImage(image,T);

               // check if the pixel has been painted in a different color now
               if (checkPixelChanged(fg)) {
                  // repainting the pixel causes a call to checkPixel to return the
                  // correct state
                  if (_fg) {
                     _impl->setStrokeColor(*_fg);
                     _impl->drawPoints(&_pixel,1);
                  }
                  else {
                     _impl->setStrokeColor(invertColor(_bg));
                     _impl->drawPoints(&_pixel,1);
                  }
               }
               else {
                  _impl->setStrokeColor(_bg);
                  _impl->drawPoints(&_pixel,1);
                  if (_fg) {
                     _impl->setStrokeColor(*_fg);
                  }
               }
            }

            /**
             * Set the current font
             * @param f the new font
             */
            void setFont(const ::timber::Pointer< ::indigo::Font>& font)
            {
               _impl->setFont(font);
            }

            /**
             * Draw a string with the current font
             */
            void drawText(const ::std::string& str, const OutPoint& pt)
            {
               if (!activateStrokeColor()) {
                  return;
               }
               _impl->drawText(str, pt);
            }

            /**
             * Activate the fill color.
             * @param c the new color
             */
         private:
            bool activateFillColor()throws()
            {
               if (checkPixelChanged(_bg)) {
                  return false;
               }

               if (_fg!=&_fill) {
                  // read the current color and make it the background color
                  _fg = &_fill;
                  // use a background color that is not the foreground color
                  _bg = invertColor(*_fg);

                  // use the stroke to clear the pixel
                  _impl->setStrokeColor(_bg);
                  _impl->drawPoints(&_pixel,1);
                  // re-read the background color to make sure we're not going to have any round-off problems
                  _bg = _impl->readPixel(_pixel);
                  _impl->setFillColor(*_fg);
               }
               return true;
            }

            /**
             * Activate the fill color.
             * @param c the new color
             */
         private:
            bool activateStrokeColor()throws()
            {
               if (checkPixelChanged(_bg)) {
                  return false;
               }

               if (_fg!=&_stroke) {

                  // read the current color and make it the background color
                  _fg = &_stroke;
                  // use a background color that is not the foreground color
                  _bg = invertColor(*_fg);

                  // use the stroke to clear the pixel
                  _impl->setStrokeColor(_bg);
                  _impl->drawPoints(&_pixel,1);
                  // re-read the background color to make sure we're not going to have any round-off problems
                  _bg = _impl->readPixel(_pixel);
                  _impl->setStrokeColor(*_fg);
               }
               return true;
            }

            /** The renderer implementation */
         private:
            ::std::unique_ptr<RENDERER> _impl;

            /** The original material color */
         private:
            const Color _original;

            /** The current background color */
         private:
            Color _bg;

            /** The current active color */
         private:
            Color* _fg;

            /** The current stroke and fill colors */
         private:
            Color _stroke, _fill;

            /** The pixel */
         private:
            OutPoint _pixel;

            /** True if the pixel had changed */
         private:
            bool _pixelChanged;
      };
   }
}

#endif
