#include <indigo/x11/X11Image.h>
#include <canopy/ByteOrder.h>
#include <timber/logging.h>
#include <cstring>

#include <X11/Xutil.h>

using namespace ::timber;
using namespace ::timber::logging;

namespace indigo {
  namespace x11 {
    namespace {
      struct DefaultImage : public X11Image {
	DefaultImage(Display* dp, Visual* visual, size_t depth, size_t w, size_t h) throws (::std::exception)
	{
	  int pad = 0;
	  if (depth<=8) {
	    pad = 8;
	  }
	  else if (depth <= 16) {
	    pad = 16;
	  }
	  else {
	    pad = 32;
	  }

	  Log logger("indigo.x11.X11Image");

	  _image = ::XCreateImage(dp,visual,
				  depth,
				  ZPixmap, // format
				  0, // offset
				  new char[w*h*(pad/8)], // no data yet
				  static_cast<unsigned int>(w),static_cast<unsigned int>(h),
				  pad,
				  w*(pad/8));
	  
	  // change the byte order for the image
	  _image->byte_order = ::canopy::isBigEndian() ? MSBFirst : LSBFirst;
	  _image->bitmap_bit_order = _image->byte_order;

	  const Level level(Level::DEBUGGING);
	  if (logger.isLoggable(level)) {
	    LogEntry(logger).level(level) << "Prepared image" << ::std::endl
					  << "  width=" << _image->width << ::std::endl
					  << "  height=" << _image->height << ::std::endl
					  << "  xoffset=" << _image->xoffset << ::std::endl
					  << "  format=" << _image->format << ::std::endl
					  << "  byte_order=" << _image->byte_order << ::std::endl
					  << "  bitmap_unit=" << _image->bitmap_unit << ::std::endl
					  << "  bitmap_bit_order=" << _image->bitmap_bit_order << ::std::endl
					  << "  bitmap_pad=" << _image->bitmap_pad << ::std::endl
					  << "  depth=" << _image->depth << ::std::endl
					  << "  bytes_per_line=" << _image->bytes_per_line << ::std::endl
					  << "  bits_per_pixel=" << _image->bits_per_pixel << ::std::endl
					  << "  red_mask=" << std::hex << _image->red_mask << ::std::endl
					  << "  green_mask=" << std::hex << _image->green_mask << ::std::endl
					  << "  blue_mask=" << std::hex << _image->blue_mask << ::std::endl
					  << doLog;
	  }

	  if (!XInitImage(_image)) {
	    delete[] _image->data;
	    _image->data = 0;
	    XDestroyImage(_image);
	    throw ::std::runtime_error("Could not initialize X11 image");
	  }
	}

	~DefaultImage() throws()
	{
	  delete[] _image->data;
	  _image->data = 0;
	  XDestroyImage(_image);
	}

	XImage* x11Image() const throws() { return _image; }
	size_t width() const throws() { return _image->width; }
	size_t height() const throws() { return _image->height; }

      private:
	::XImage* _image;
      };
    }

    X11Image::X11Image() throws()
    {}

    X11Image::~X11Image() throws()
    {}

    ::std::unique_ptr<X11Image> X11Image::create(Display* dp, Visual* visual, size_t depth, size_t w, size_t h) throws (::std::exception)
    {
      return ::std::unique_ptr<X11Image>(new DefaultImage(dp,visual,depth,w,h));
    }

    
  }
}

