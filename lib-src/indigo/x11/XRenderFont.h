#ifndef _INDIGO_X11_XRENDERFONT_H
#define _INDIGO_X11_XRENDERFONT_H

#ifndef _INDIGO_X11_X11FONT_H
#include <indigo/x11/X11Font.h>
#endif

#include <X11/Xft/Xft.h>

namespace indigo {
  namespace x11 {
    
    /**
     * This class represents an XftFont
     */
    class XRenderFont : public X11Font {
      XRenderFont(const XRenderFont&);
      XRenderFont&operator=(const XRenderFont&);

      /** Default constructor. */
    protected:
      XRenderFont() throws();

      /** The destructor */
    public:
      ~XRenderFont() throws();

      /**
       * Get the X11 font implementation.
       * @return the X11 font implementation
       */
    public:
      virtual ::XftFont* xftFont() const throws() = 0;
    };
      
  }
}

#endif
