#ifndef _INDIGO_X11_PIXMAPRENDERER_H
#define _INDIGO_X11_PIXMAPRENDERER_H

#ifndef _INDIGO_X11_X11RENDERER_H
#include <indigo/x11/X11Renderer.h>
#endif

#ifndef _TIMBER_LOGGING_H
#include <timber/logging.h>
#endif

namespace indigo {
  namespace x11 {
    class X11Font;
    class X11RenderContext;

    /**
     * This class represents is a renderer for graphics into
     * a Pixmap.
     * @date 07 Jun 2003
     */
    class PixmapRenderer : public ::timber::Counted {

      /**
       * Create a renderer. If no GC is specified a default GC will be created.
       * @param screen a screen
       * @param w the pixmap's width
       * @param h the pixmap's height
       * @param gc the graphic context to be used (or 0)
       * @param colors a color map
       * @param bg the background color (clear color)
       * @param fg the foreground color 
       * @throws ::std::exception if the pixmap could not be allocated
       */
    public:
      PixmapRenderer (Screen* screen, Int w, Int h, GC gc, ::Colormap colors, const ::indigo::Color&  bg, const ::indigo::Color&  fg) throws (::std::exception);
      
      /** Destroy this renderer. */
    public:
      ~PixmapRenderer() throws();

	
      /**
       * Create a renderer. If no GC is specified a default GC will be created.
       * @param screen a screen
       * @param w the pixmap's width
       * @param h the pixmap's height
       * @param gc the graphic context to be used (or 0)
       * @param colors a color map
       * @param bg the background color (clear color)
       * @param fg the foreground color 
       * @throws ::std::exception if the pixmap could not be allocated
       */
    public:
      static ::timber::Reference<PixmapRenderer> create (Screen* screen, Int w, Int h, GC gc, ::Colormap& colors, 
							 const ::indigo::Color&  bg,
							 const ::indigo::Color&  fg) throws (::std::exception);


      /** Destroy this renderer */
    protected:
      void destroy() throws();
      
      /**
       * Resize this renderer. If an exception is thrown, then the 
       * renderer will remain usable, but not have the requested size.
       * The current pixmap will be invalidated!
       * @param w the pixmap's width
       * @param h the pixmap's height
       * @return true if the size of the pixmap was set to w,h
       */
    public:
      bool resize (Int w, Int h) throws();
	
      /**
       * Get the width of the pixmap.
       * @return pixmap's width
       */
    public:
      inline Int width() const throws() { return _width; }
	
      /**
       * Get the height of the pixmap.
       * @return pixmap's height
       */
    public:
      inline Int height() const throws() { return _height; }
	
      /**
       * Set the graphic which is to be rendered into the pixmap.
       * @param g a graphic or null to clear the current graphic
       */
    public:
      void setGraphic (const ::timber::Reference<Node>& g) throws();

      /**
       * Set the default colors. The pixmap is automatically refreshed.
       * @param bg the default background material
       * @param fg the default foreground material
       */
    public:
      void setDefaultColors (const ::indigo::Color& bg, const ::indigo::Color&  fg) throws();

      /**
       * Refresh the pixmap to the most recent state of the graphic.
       */
    public:
      void refresh () throws();

      /**
       * Set the default font.
       * @param font  a default font or 0
       */
    public:
      void setDefaultFont (const ::timber::Pointer<X11Font>& font) throws();

      /**
       * Get the currently set default font.
       * @return the default font
       */
    public:
      ::timber::Pointer<X11Font> defaultFont() const throws();

      /**
       * Get the pixmap. Do not destroy the pixmap.
       * @return the pixmap created by this renderer
       */
    public:
      inline Pixmap pixmap() const throws()
      { return _pixmap; }

      /** Clear the pixmap */
    private:
      void clearPixmap() throws();

      /** A logger */
    private:
      ::timber::logging::Log _logger;

      /** A render context */
    private:
      ::timber::Pointer<X11RenderContext> _renderContext;

      /** True if the GC must be destroyed */
    private:
      bool _destroyGC;

      /** The pixmap */
    private:
      mutable Pixmap _pixmap;

      /** The width of the pixmap */
    private:
      Int _width;

      /** The height of the pixmap */
    private:
      Int _height;
	
      /** The depth */
    private:
      Int _depth;
	
      /** The background material */
    private:
     ::indigo::Color _bg;

      /** The foreground material */
    private:
     ::indigo::Color _fg;

      /** The background pixel color */
    private:
     Pixel _bgPixel;

      /** The foreground pixel color */
    private:
      Pixel _fgPixel;

      /** The graphic */
    private:
      ::timber::Pointer<Node> _graphic;

      /** The default font (if set) */
    private:
      ::timber::Pointer<X11Font> _defaultFont;
   };
  }
}

#endif
