#ifndef _INDIGO_X11_X11IMAGE_H
#define _INDIGO_X11_X11IMAGE_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#include <X11/Xlib.h>
#include <memory>

namespace indigo {
  namespace x11 {
    
    /**
     * This abstract class extends the image ID interface with
     * a method to obtain an X11 image.
     */
    class X11Image {
      X11Image(const X11Image&);
      X11Image&operator=(const X11Image&);
      
      /** Default constructor. */
    protected:
      X11Image() throws();

      /** The destructor */
    public:
      virtual ~X11Image() throws() = 0;
     
      /**
       * Get the width of this image in pixels.
       * @return the width of this image
       */
    public:
      virtual size_t width() const throws() = 0;
      
      /**
       * Get the height of this image in pixels
       * @return the height of this image
       */
    public:
      virtual size_t height() const throws() = 0;
      
      /**
       * Create a default X11 image. The data will
       * be owned by the created image and will deleted
       * in the constructor.
       *
       * @param dp a display
       * @param visual the visual for the image
       * @param depth the depth for the image
       * @param w the image widith
       * @param h the image height
       * @return an X11 image
       * @throws ::std::exception if the image could not be created
       */
    public:
      static ::std::unique_ptr<X11Image> create(Display* dp, Visual* visual, size_t depth, size_t w, size_t h) throws (::std::exception);
      
      /**
       * Get the X11 image implementation.
       * @return the X11 image implementation
       */
    public:
      virtual XImage* x11Image() const throws() = 0;
    };
      
  }
}

#endif
