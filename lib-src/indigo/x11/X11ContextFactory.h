#ifndef _INDIGO_X11_X11CONTEXTFACTORY_H
#define _INDIGO_X11_X11CONTEXTFACTORY_H

#ifndef _XLIB_H_
#include <X11/Xlib.h>
#endif

#ifndef _INDIGO_RENDER_CONTEXT_H
#include <indigo/render/Context.h>
#endif

#ifndef _INDIGO_RENDER_PICKCONTEXT_H
#include <indigo/render/PickContext.h>
#endif

#ifndef _INDIGO_PICKINFO_H
#include <indigo/PickInfo.h>
#endif

#ifndef _INDIGO_COLOR_H
#include <indigo/Color.h>
#endif

#ifndef _INDIGO_X11_UTIL_H
#include <indigo/x11/util.h>
#endif

#ifndef _TIMBER_LOGGING_H
#include <timber/logging.h>
#endif

namespace indigo {
   namespace x11 {
      class X11Font;
      class X11Image;
      class X11RenderContext;
      class PixmapRenderContext;

      /**
       * A graphics renderer that is capable of rendering
       * a 2d scene graph into a drawable. Drawing is accomplished
       * through standard X11 drawing methods. The render is not
       * threadsafe.<p>
       * Currently X11ContextFactory can use either X11 rendering or XRender based rendering. When using
       * direct X11 rendering, lines are draw very fast, but at the expense of thing like transparency. Use
       * the environment variable {@link INDIGO_X11_XRENDERER} to enable or disable the Render extension.
       * The default enables the use of the Render extension.
       */
      class X11ContextFactory : public ::timber::Counted
      {
            X11ContextFactory();
            X11ContextFactory&operator=(const X11ContextFactory&);
            X11ContextFactory(const X11ContextFactory&);

            /**
             * A pick info object
             */
         public:
            struct PickInfo
            {
                  // the picked pixel
                  size_t x, y;

                  // the info node closest to the picked node
                  ::timber::Pointer< ::indigo::PickInfo> info;
                  // the picked node
                  ::timber::Pointer< ::indigo::Node> picked;
                  // the shape that was picked if the node was composite node or a shapes node
                  size_t detail;
            };

            /**
             * Create a new renderer using the specified drawable.
             * @param dpy a display
             * @param d a drawable
             * @param gc a graphics context
             * @param colors a color map
             * @param defColor the default pixel color
             */
         private:
            X11ContextFactory(const ::timber::Reference< X11RenderContext>& ctx, const ::indigo::Color& defColor)throws();

            /**
             * Create a new renderer using the specified drawable.
             * @param dpy a display
             * @param d a drawable
             * @param gc a graphics context
             * @param colors a color map
             * @param defColor the default pixel color
             */
         public:
            static ::timber::Reference< X11ContextFactory> create(const ::timber::Reference< X11RenderContext>& ctx,
                  const ::indigo::Color& defColor)throws();

            /** Destroy this renderer */
         public:
            virtual ~X11ContextFactory()throws();

            /**
             * Set the default font.
             * @param font  a default font or 0
             */
         public:
            void setDefaultFont(const ::timber::Pointer< X11Font>& font)throws();

            /**
             * Get the currently set default font.
             * @return the default font
             */
         public:
            ::timber::Pointer< X11Font> defaultFont() const throws();

            /**
             * Create the context implementation.
             * @param contextWrapper a function takes a renderer and creates a new renderer
             * @param x the x-coordinate of the top-left corner
             * @param y the y-coordinate of the top-left corner
             * @param w the width of the clip rectangle
             * @param h the height of the clip rectangle
             * @param ul upper-left corner of clip rectangle
             * @param lr lower-right corner of clip rectangle
             * @return a new context
             */
         private:
            template <class T>
            ::std::unique_ptr<typename T::Result> createContextImpl(const T& contextWrapper,
            double x, double y, double w, double h, const Point& ul, const Point& ur)throws();

            /**
             * Create a new context for this renderer.
             * @param x the x-coordinate of the top-left corner
             * @param y the y-coordinate of the top-left corner
             * @param w the width of the clip rectangle
             * @param h the height of the clip rectangle
             * @return a new context
             */
         public:
            ::std::unique_ptr< ::indigo::render::Context> createContext(double x, double y, double w, double h)throws();

            /**
             * Create a new context for this renderer.
             * @param x the x-coordinate of the top-left corner
             * @param y the y-coordinate of the top-left corner
             * @param w the width of the clip rectangle
             * @param h the height of the clip rectangle
             * @param pickX the pixel to be picked
             * @param pickY the pixel to be picked
             * @return a new context
             */
         public:
            ::std::unique_ptr< ::indigo::render::PickContext> createPickContext(double x, double y, double w, double h,
                  size_t pickX, size_t pickY)throws();

            /** The render context */
         private:
            const ::timber::Reference< X11RenderContext> _renderContext;

            /** The default pixel color */
         private:
            const ::indigo::Color _defaultColor;

            /** The default font (if set) */
         private:
            ::timber::Pointer< X11Font> _defaultFont;

            /** A logger */
         private:
            ::timber::logging::Log _logger;

            /** The Render extension is available if set */
         private:
            bool _renderExtension;

      };
   }
}

#endif
