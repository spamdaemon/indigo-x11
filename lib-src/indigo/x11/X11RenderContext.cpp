#include <indigo/x11/X11RenderContext.h>

using namespace ::std;

namespace indigo {
   namespace x11 {

      X11RenderContext::X11RenderContext(::Display* dp, ::GC gcontext, ::Drawable canvas, ::Colormap colormap)
      throws()
      : _display(dp), _canvas(canvas),_gc(gcontext),_colors(colormap)
      {
         assert(dp != 0);
         assert(gcontext!=0);
         assert(canvas!=0);
         assert(colormap!=0);
      }

      X11RenderContext::~X11RenderContext()
throws   ()
   {}
}

}
