#include <indigo/x11/PixmapRenderer.h>
#include <indigo/x11/X11RenderContext.h>
#include <indigo/x11/X11Font.h>
#include <iostream>

using namespace ::timber;
using namespace ::timber::logging;

namespace indigo {
   namespace x11 {

      PixmapRenderer::PixmapRenderer(Screen* screen, Int w, Int h, ::GC gc, ::Colormap colors,
            const ::indigo::Color& bg, const ::indigo::Color& fg)
            throws (::std::exception)
            : _logger("indigo.x11.PixmapRenderer"), _destroyGC(false), _width(w), _height(h), _depth(
                  DefaultDepthOfScreen(screen)), _bg(bg), _fg(fg), _bgPixel(
                  convertColorToPixel(DisplayOfScreen(screen), colors, bg)), _fgPixel(
                  convertColorToPixel(DisplayOfScreen(screen), colors, fg))
      {
         ::Display* dpy = DisplayOfScreen(screen);

         LogEntry(_logger).debugging() << "Create with size " << w << "x" << h << doLog;
         Drawable d = RootWindowOfScreen(screen);
         _pixmap = ::XCreatePixmap(dpy, d, w, h, _depth);
         if (_pixmap == 0) {
            throw ::std::runtime_error("Could not allocate pixmap");
         }
         if (gc == 0) {
            gc = ::XCreateGC(dpy, d, 0, 0);
            _destroyGC = true;
         }
         _renderContext = new X11RenderContext(dpy, gc, _pixmap, colors);
      }

      PixmapRenderer::~PixmapRenderer()
      throws()
      {
         if (_destroyGC) {
            ::XFreeGC(_renderContext->display(), _renderContext->gc());
         }

         XFreePixmap(_renderContext->display(), _pixmap);
      }

      Reference< PixmapRenderer> PixmapRenderer::create(Screen* screen, Int w, Int h, ::GC gc, ::Colormap& colors,
            const ::indigo::Color& bg, const ::indigo::Color& fg)
            throws (::std::exception)
      {
         return new PixmapRenderer(screen, w, h, gc, colors, bg, fg);
      }

      void PixmapRenderer::destroy() throws()
      {
         delete this;
      }

      void PixmapRenderer::refresh() throws()
      {
         clearPixmap();
         if (_graphic != nullptr) {
            Reference< X11Renderer> r = X11Renderer::create(_renderContext, _fg);
            r->setDefaultFont(_defaultFont);
            r->render(_graphic, 0, 0, _width, _height);
         }
      }

      bool PixmapRenderer::resize(Int w, Int h) throws()
      {
         if (_width != w || _height != h) {
            LogEntry(_logger).debugging() << "Resize from " << _width << "x" << _height << " to  " << w << "x" << h
                  << doLog;
            Pixmap p = XCreatePixmap(_renderContext->display(), _pixmap, w, h, _depth);
            if (p == 0) {
               ::std::cerr << "Could not create pixmap (" << w << " by " << h << ::std::endl;
               return false;
            }

            _width = w;
            _height = h;
            XFreePixmap(_renderContext->display(), _pixmap);
            _pixmap = p;
            _renderContext = new X11RenderContext(_renderContext->display(), _renderContext->gc(), _pixmap,
                  _renderContext->colors());

            ::XRectangle clipRects[1];
            clipRects[0].x = static_cast< Short>(0);
            clipRects[0].y = static_cast< Short>(0);
            clipRects[0].width = static_cast< UShort>(w - 1);
            clipRects[0].height = static_cast< UShort>(h - 1);
            ::XSetClipRectangles(_renderContext->display(), _renderContext->gc(), 0, 0, clipRects, 1, Unsorted);
         }
         return true;
      }

      void PixmapRenderer::setGraphic(const Reference< Node>& g) throws()
      {
         _graphic = g;
      }

      void PixmapRenderer::setDefaultColors(const ::indigo::Color& bg, const ::indigo::Color& fg) throws()
      {
         _fg = fg;
         _bg = bg;
         _bgPixel = convertColorToPixel(_renderContext->display(), _renderContext->colors(), bg);
         _fgPixel = convertColorToPixel(_renderContext->display(), _renderContext->colors(), fg);
      }

      void PixmapRenderer::clearPixmap() throws()
      {
         ::XSetBackground(_renderContext->display(), _renderContext->gc(), _bgPixel);
         ::XSetForeground(_renderContext->display(), _renderContext->gc(), _bgPixel);
         ::XFillRectangle(_renderContext->display(), _renderContext->drawable(), _renderContext->gc(), 0, 0, _width,
               _height);
         ::XSetForeground(_renderContext->display(), _renderContext->gc(), _fgPixel);
      }

      void PixmapRenderer::setDefaultFont(const Pointer< X11Font>& font) throws()
      {
         _defaultFont = font;
      }

      Pointer< X11Font> PixmapRenderer::defaultFont() const throws()
      {
         return _defaultFont;
      }

   }
}
