#include <indigo/x11/XRenderer.h>
#include <indigo/x11/X11RenderContext.h>
#include <indigo/x11/XRenderFont.h>
#include <indigo/x11/X11Image.h>
#include <indigo/x11/util.h>
#include <indigo/Color.h>
#include <indigo/Point.h>
#include <indigo/AffineTransform2D.h>
#include <timber/logging.h>

#include <cmath>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace indigo {
   namespace x11 {

      namespace {

         static Log logger()
         {
            return Log("indigo.x11.XRenderer");
         }

         static void tessError(XRenderer* renderer)
         {
            logger().warn("Tesselation error");
         }

         static void updateBounds(const XTrap& t, XRenderer::OutPoint& ul, XRenderer::OutPoint& lr)
         {
            if (t.top.y < ul.y) {
               ul.y = t.top.y;
            }
            if (t.bottom.y > lr.y) {
               lr.y = t.bottom.y;
            }
            if (t.top.left < ul.x) {
               ul.x = t.top.left;
            }
            if (t.bottom.left < ul.x) {
               ul.x = t.bottom.left;
            }
            if (t.top.right > lr.x) {
               lr.x = t.top.right;
            }
            if (t.bottom.right > lr.x) {
               lr.x = t.bottom.right;
            }
         }

         static XRenderColor* convertColor(const Color& c, double a, XRenderColor& x)
         {
            const Color col=c.getPremultipliedColor();
            col.rgba16(x.red, x.green, x.blue,x.alpha);
            return &x;
         }

         /**
          * Create one or more trapezoids representing a line of the specified width. If bot->y < top->y
          * the top and bottom are swapped.
          * @param bot the bottom point of the trapezoid
          * @param top the top point of the trapezoid
          * @param w the width of the line
          * @param t a pointer to at least 3 trapezoids
          * @return the number of generated trapezoids
          */
         static size_t makeLine(const XRenderer::OutPoint* P, const XRenderer::OutPoint* Q, XFixed wFixed, XTrap* t,
               XRenderer::OutPoint& ul, XRenderer::OutPoint& lr)
         {
            if ((P->y < Q->y)) {
               ::std::swap(P, Q);
            }
            else if (P->y == Q->y) {
               // horizontal segment
               if (P->x > Q->x) {
                  ::std::swap(P, Q);
               }

               t->bottom.y = P->y + wFixed / 2;
               t->top.y = t->bottom.y - wFixed;
               t->top.left = t->bottom.left = P->x;
               t->top.right = t->bottom.right = Q->x;
               updateBounds(*t, ul, lr);
               return 1;
            }

            // vertical segment
            if (P->x == Q->x) {
               t->top.y = Q->y;
               t->bottom.y = P->y;
               t->bottom.left = P->x - wFixed / 2;
               t->bottom.right = t->bottom.left + wFixed;
               t->top.left = t->bottom.left;
               t->top.right = t->bottom.right;
               updateBounds(*t, ul, lr);
               return 1;
            }

            //
            XFixed dxFixed = P->x - Q->x;
            XFixed dyFixed = P->y - Q->y;

            double dx = XFixedToDouble(dxFixed);
            double dy = XFixedToDouble(dyFixed);
            double len = ::std::sqrt(dx * dx + dy * dy);

            // the length of the segment H in the
            double w = XFixedToDouble(wFixed);

            // the (wx,wy) vector is constructed, so that it lies on the right hand side
            // of the vector (dx,dy)
            double wx = dy / len;
            double wy = -dx / len;
            double hx = -wy;

            if (wy < 0) {
               wx = -wx;
               wy = -wy;
            }

            // we may still have a problem if w is HUGE
            XFixed wxFixed = XDoubleToFixed(wx * (0.5 * w));
            XFixed wyFixed = XDoubleToFixed(wy * (0.5 * w));

            // the normal case, where the length is dominates the width of the line
            XFixed topx = Q->x - wxFixed;
            XFixed topy = Q->y - wyFixed;
            XFixed botx = P->x + wxFixed;
            XFixed boty = P->y + wyFixed;

            // we need to swap the orientation
            if ((P->y - wyFixed) <= (Q->y + wyFixed)) {
               XFixed hxFixed = XDoubleToFixed(::std::abs(len * len / dx));

               t->top.y = topy + dyFixed;
               t->bottom.y = boty - dyFixed;
               if (dx > 0) {
                  t->top.right = topx + dxFixed;
                  t->top.left = t->top.right - hxFixed;
                  t->bottom.left = botx - dxFixed;
                  t->bottom.right = t->bottom.left + hxFixed;
               }
               else {
                  t->top.left = topx + dxFixed;
                  t->top.right = t->top.left + hxFixed;
                  t->bottom.right = botx - dxFixed;
                  t->bottom.left = t->bottom.right - hxFixed;
               }
            }
            else {
               XFixed hxFixed = XDoubleToFixed(::std::abs(w * dx / dy) * hx);

               t->top.y = Q->y + wyFixed;
               t->bottom.y = P->y - wyFixed;

               if (hx < 0) {
                  t->top.right = Q->x + wxFixed;
                  t->top.left = topx + hxFixed;
                  t->bottom.left = P->x - wxFixed;
                  t->bottom.right = botx - hxFixed;
               }
               else {
                  t->top.left = Q->x + wxFixed;
                  t->top.right = topx + hxFixed;
                  t->bottom.right = P->x - wxFixed;
                  t->bottom.left = botx - hxFixed;
               }
            }
            assert(t->top.left <= t->top.right);
            assert(t->bottom.left <= t->bottom.right);
            assert(t->top.y <= t->bottom.y);

            t[1].bottom = t->top;
            t[1].top.y = topy;
            t[1].top.right = t[1].top.left = topx;

            assert(t[1].top.left <= t[1].top.right);
            assert(t[1].bottom.left <= t[1].bottom.right);
            assert(t[1].top.y <= t[1].bottom.y);
            assert(t[1].bottom.y <= t->top.y);

            t[2].bottom.y = boty;
            t[2].bottom.right = t[2].bottom.left = botx;
            t[2].top = t->bottom;

            assert(t[2].top.left <= t[2].top.right);
            assert(t[2].bottom.left <= t[2].bottom.right);
            assert(t[2].top.y <= t[2].bottom.y);
            assert(t[2].top.y >= t->bottom.y);

            updateBounds(t[1], ul, lr);
            updateBounds(t[2], ul, lr);
            return 3;
         }

         static ::XPoint convertToXPoint(const XPointFixed& pts)
         {
            ::XPoint res;
            res.x = static_cast< short> (XFixedToDouble(pts.x));
            res.y = static_cast< short> (XFixedToDouble(pts.y));
            return res;
         }

         static void convertToXPoints(const XPointFixed* pts, size_t n, ::std::vector< ::XPoint>& out)
         {
            out.resize(n);
            for (::std::vector< ::XPoint>::iterator i = out.begin(), end = out.end(); i != end; ++i, ++pts) {
               i->x = static_cast< short> (XFixedToDouble(pts->x));
               i->y = static_cast< short> (XFixedToDouble(pts->y));
            }
         }

      }

      XRenderer::Brush::Brush(::Display* dpy)
      throws() :
      _display(dpy), _changeFlags(ALL), _alpha(1),_aaMode(AntiAliasingMode::NONE)
      {
         _format = XRenderFindStandardFormat(dpy, PictStandardARGB32);
         // create a temporary pixmap, just so that we can draw into it; we won't need it once we're done
         // the pixmap is 1x1 and will only contain the color
         _drawable = XCreatePixmap(dpy, DefaultRootWindow(dpy), 1, 1, _format->depth);
         // the pixel will be repeated as necessary in operations
         XRenderPictureAttributes attrs;
         attrs.repeat = RepeatNormal;
         _pict = XRenderCreatePicture(dpy, _drawable, _format, CPRepeat, &attrs);
      }

      XRenderer::Brush::~Brush()
      throws()
      {
         XFreePixmap(_display, _drawable);
         XRenderFreePicture(_display, _pict);
      }

      XRenderer::Brush::Visibility XRenderer::Brush::visibility() const
      throws()
      {
         short v = (short) (_color.opacity() * _alpha * 0xffff);
         if (v == 0) {
            return INVISIBLE;
         }
         if (v == (short) 0xffff) {
            return OPAQUE;
         }
         return TRANSLUCENT;
      }

      Picture XRenderer::Brush::operator()() const
      throws()
      {
         if (_changeFlags != 0) {
            // apply any changes
            XRenderColor xcolor;
            XRenderFillRectangle(_display, PictOpSrc, _pict, convertColor(_color, _alpha, xcolor), 0, 0, 1, 1);
            _changeFlags = 0;
         }
         return _pict;
      }

      void XRenderer::Brush::setAlpha(double alpha)
      throws()
      {
         if (_alpha != alpha) {
            _alpha = alpha;
            _changeFlags |= ALPHA;
         }
      }

      void XRenderer::Brush::setColor(const Color& c)
      throws()
      {
         if (_color != c) {
            _color = c;
            _changeFlags |= COLOR;
         }
      }
      void XRenderer::Brush::setAAMode(AntiAliasingMode m)
      throws()
      {
         if (_aaMode != m) {
            _aaMode = m;
            _changeFlags |= AAMODE;
         }
      }

      XRenderer::Pen::Pen(::Display* dpy)
      throws() :
      _display(dpy), _changeFlags(ALL), _alpha(1), _width(1), _color(Color::BLACK),_aaMode(AntiAliasingMode::FAST)
      {
         _format = XRenderFindStandardFormat(dpy, PictStandardARGB32);
         // create a temporary pixmap, just so that we can draw into it; we won't need it once we're done
         // the pixmap is 1x1 and will only contain the color
         _drawable = XCreatePixmap(dpy, DefaultRootWindow(dpy), 1, 1, _format->depth);
         // the pixel will be repeated as necessary in operations
         XRenderPictureAttributes attrs;
         attrs.repeat = RepeatNormal;
         _pict = XRenderCreatePicture(dpy, _drawable, _format, CPRepeat, &attrs);
      }

      XRenderer::Pen::~Pen()
      throws()
      {
         XFreePixmap(_display, _drawable);
         XRenderFreePicture(_display, _pict);
      }

      XRenderer::Pen::Visibility XRenderer::Pen::visibility() const
      throws()
      {
         short v = (short) (_color.opacity() * _alpha * 0xffff);
         if (v == 0) {
            return INVISIBLE;
         }
         if (v == (short) 0xffff) {
            return OPAQUE;
         }
         return TRANSLUCENT;
      }

      Picture XRenderer::Pen::operator()() const
      throws()
      {
         if (_changeFlags != 0) {
            // apply any changes
            XRenderColor xcolor;
            XRenderFillRectangle(_display, PictOpSrc, _pict, convertColor(_color, _alpha, xcolor), 0, 0, 1, 1);
            _changeFlags = 0;
         }
         return _pict;
      }

      void XRenderer::Pen::setAlpha(double alpha)
      throws()
      {
         if (_alpha != alpha) {
            _alpha = alpha;
            _changeFlags |= ALPHA;
         }
      }

      void XRenderer::Pen::setWidth(double w)
      throws()
      {
         if (_width != w) {
            _width = w;
            _changeFlags |= WIDTH;
         }
      }

      void XRenderer::Pen::setAAMode(AntiAliasingMode m)
      throws()
      {
         if (_aaMode != m) {
            _aaMode = m;
            _changeFlags |= AAMODE;
         }
      }

      void XRenderer::Pen::setColor(const Color& c)
      throws()
      {
         if (_color != c) {
            _color = c;
            _changeFlags |= COLOR;
         }
      }

      XRenderer::Alpha::Alpha(::Display* dpy, size_t w, size_t h, AntiAliasingMode aaMode)
      throws() :
      _display(dpy), _changeFlags(ALL), _alpha(1), _width(w), _height(h)
      {
         switch(aaMode) {
            case AntiAliasingMode::NONE:
            _format = XRenderFindStandardFormat(dpy, PictStandardA1);
            break;
            default:
            _format = XRenderFindStandardFormat(dpy, PictStandardA8);
            break;
         };

         // create a temporary pixmap, just so that we can draw into it; we won't need it once we're done
         // the pixmap is 1x1 and will only contain the color
         _drawable = XCreatePixmap(dpy, DefaultRootWindow(dpy), w, h, _format->depth);
         // the pixel will be repeated as necessary in operations
         XRenderPictureAttributes attrs;
         attrs.poly_edge = PolyEdgeSharp;
         attrs.poly_mode = PolyModePrecise;
         attrs.repeat = RepeatNormal;
         _pict = XRenderCreatePicture(dpy, _drawable, _format, CPRepeat/*|CPPolyEdge|CPPolyMode*/, &attrs);
         clear();
      }

      XRenderer::Alpha::~Alpha()
      throws()
      {
         XFreePixmap(_display, _drawable);
         XRenderFreePicture(_display, _pict);
      }

      XRenderer::Alpha::Visibility XRenderer::Alpha::visibility() const
      throws()
      {
         short v = (short) (_alpha * 0xffff);
         if (v == 0) {
            return INVISIBLE;
         }
         if (v == (short) 0xffff) {
            return OPAQUE;
         }
         return TRANSLUCENT;
      }

      void XRenderer::Alpha::clear()
      throws()
      {
         // apply any changes
         XRenderColor xcolor;
         xcolor.red = 0;
         xcolor.green = 0;
         xcolor.blue = 0;
         xcolor.alpha = (UShort) (_alpha * 0xffff);
         XRenderFillRectangle(_display, PictOpSrc, _pict, &xcolor, 0, 0, _width, _height);
         _changeFlags = 0;
      }

      Picture XRenderer::Alpha::operator()() const
      throws()
      {
         if (_changeFlags != 0) {
            const_cast< Alpha&> (*this).clear();
         }
         return _pict;
      }

      void XRenderer::Alpha::setAlpha(double alpha)
      throws()
      {
         if (_alpha != alpha) {
            _alpha = alpha;
            _changeFlags |= ALPHA;
         }
      }

      XRenderer::FontPen::FontPen(::Display* dpy, ::Drawable c, ::Colormap colormap)
      throws() :
      _display(dpy), _changeFlags(ALL),_aaMode(AntiAliasingMode::FAST), _alpha(1), _color(Color::WHITE), _colors(colormap)
      {
         Visual* visual = DefaultVisual(dpy, DefaultScreen(dpy));
         _fontContext = XftDrawCreate(dpy, c, visual, colormap);

         // allocate some default color (black)
         XRenderColor xcolor;
         xcolor.red = xcolor.blue = xcolor.green = xcolor.alpha = 0xffff;
         XftColorAllocValue(_display, visual, _colors, &xcolor, &_fontColor);
      }

      XRenderer::FontPen::~FontPen()
      throws()
      {
         Visual* visual = DefaultVisual(_display, DefaultScreen(_display));
         XftColorFree(_display, visual, _colors, &_fontColor);
         XftDrawDestroy(_fontContext);
      }

      XRenderer::FontPen::Visibility XRenderer::FontPen::visibility() const
      throws()
      {
         short v = (short) (_color.opacity() * _alpha * 0xffff);
         if (v == 0) {
            return INVISIBLE;
         }
         if (v == (short) 0xffff) {
            return OPAQUE;
         }
         return TRANSLUCENT;
      }

      XftColor* XRenderer::FontPen::fontColor() const
      throws()
      {
         (*this)(); // rebuild the context
         return &_fontColor;
      }

      XftDraw* XRenderer::FontPen::operator()() const
      throws()
      {
         if (_changeFlags != 0) {
            // apply any changes
            Visual* visual = DefaultVisual(_display, DefaultScreen(_display));
            XftColorFree(_display, visual, _colors, &_fontColor);
            XRenderColor xcolor;
            XftColorAllocValue(_display, visual, _colors, convertColor(_color, _alpha, xcolor), &_fontColor);

            _changeFlags = 0;
         }
         return _fontContext;
      }

      void XRenderer::FontPen::setFont(const ::timber::Pointer< ::indigo::x11::XRenderFont>& fnt)
      throws()
      {
         _font = fnt;
      }

      void XRenderer::FontPen::setAAMode(::indigo::AntiAliasingMode xaaMode)
      throws()
      {
         if (_aaMode != xaaMode) {
            _aaMode = xaaMode;
            _changeFlags |= AAMODE;
         }
      }

      void XRenderer::FontPen::setAlpha(double alpha)
      throws()
      {
         if (_alpha != alpha) {
            _alpha = alpha;
            _changeFlags |= ALPHA;
         }
      }

      void XRenderer::FontPen::setColor(const Color& c)
      throws()
      {
         if (_color != c) {
            _color = c;
            _changeFlags |= COLOR;
         }
      }

      XRenderer::XRenderer(const ::timber::Reference<X11RenderContext>& ctx, size_t w, size_t h)
      throws() :
      _width(w), _height(h), _ctx(ctx), _legacy(ctx),
      _brush(ctx->display()), _pen(ctx->display()), _fontPen(ctx->display(), ctx->drawable(), ctx->colors()),
      _mask1x1(ctx->display(), 1, 1,AntiAliasingMode::FAST), _mask(ctx->display(), w, h,AntiAliasingMode::FAST),
      _maskAANone(ctx->display(), w, h,AntiAliasingMode::NONE)
      {
         XRenderPictureAttributes attrs;

         _pen.setAAMode(AntiAliasingMode::FAST);
         _brush.setAAMode(AntiAliasingMode::FAST);
         _fontPen.setAAMode(AntiAliasingMode::FAST);

         Visual* visual = DefaultVisual(ctx->display(), DefaultScreen(ctx->display()));
         // some systems may not support this call because they are old systems; that's just too bad
         _destFormat = XRenderFindVisualFormat(ctx->display(), visual);
         attrs.poly_edge = PolyEdgeSharp;
         attrs.poly_mode = PolyModePrecise;
         _dest = XRenderCreatePicture(ctx->display(), ctx->drawable(), _destFormat, CPPolyEdge | CPPolyMode, &attrs);

         // create a pixmap into which images can be rendered
         _imageWidth = _width;
         _imageHeight = _height;
         _imageDrawable = XCreatePixmap(ctx->display(), DefaultRootWindow(ctx->display()), _imageWidth, _imageHeight, _destFormat->depth);
         _image = XRenderCreatePicture(ctx->display(), _imageDrawable, _destFormat, 0, &attrs);
         _imageGC = ::XCreateGC(ctx->display(), _imageDrawable, 0, 0);

         // setup the tesselator
         _gluTess._tesselator = gluNewTess();
         gluTessCallback(_gluTess._tesselator, GLU_TESS_VERTEX_DATA, (GLvoid(*)()) &addVertex);
         gluTessCallback(_gluTess._tesselator, GLU_TESS_BEGIN_DATA, (GLvoid(*)()) &beginObject);
         gluTessCallback(_gluTess._tesselator, GLU_TESS_END_DATA, (GLvoid(*)()) &endObject);
         gluTessCallback(_gluTess._tesselator, GLU_TESS_ERROR_DATA, (GLvoid(*)()) &tessError);
      }

      XRenderer::~XRenderer()
      throws()
      {
         gluDeleteTess(_gluTess._tesselator);

         XFreeGC(_ctx->display(), _imageGC);
         XFreePixmap(_ctx->display(), _imageDrawable);
         XRenderFreePicture(_ctx->display(), _image);
         XRenderFreePicture(_ctx->display(), _dest);
      }

      void XRenderer::setFillColor(const Color& c)
      throws()
      {
         _legacy.setFillColor(c);
         _brush.setColor(c);
      }

      void XRenderer::setStrokeColor(const Color& c)
      throws()
      {
         _legacy.setStrokeColor(c);
         _pen.setColor(c);
         _fontPen.setColor(c);
      }

      void XRenderer::setStrokeWidth(double w)
      throws()
      {
         _legacy.setStrokeWidth(w);
         _pen.setWidth(w);
      }

      void XRenderer::setAntiAliasingMode(::indigo::AntiAliasingMode mode)
      throws()
      {
         _legacy.setAntiAliasingMode(mode);
         _pen.setAAMode(mode);
         _brush.setAAMode(mode);
         _fontPen.setAAMode(mode);
      }

      void XRenderer::setAlpha(double a)
      throws()
      {
         _legacy.setAlpha(a);
         _brush.setAlpha(a);
         _pen.setAlpha(a);
         _fontPen.setAlpha(a);
         _mask.setAlpha(a);
         _mask1x1.setAlpha(a);
         _maskAANone.setAlpha(a);
      }

      XRenderer::OutPoint
      XRenderer::offsetPoint(const OutPoint& pt, ::timber::Short offsetX, ::timber::Short offsetY) const
      throws()
      {
         OutPoint xp;
         xp.x = pt.x + XDoubleToFixed(offsetX);
         xp.y = pt.y + XDoubleToFixed(offsetY);
         return xp;
      }

      XRenderer::OutPoint XRenderer::makePoint(const Point& pt) const
      throws()
      {
         OutPoint xp;
         makePoint(pt, xp);
         return xp;
      }

      void XRenderer::makePoint(const Point& in, OutPoint& out) const
      throws()
      {
         double x = ::std::min(32768.0, ::std::max(-32768., in.x()));
         out.x = XDoubleToFixed(x);
         double y = ::std::min(32768.0, ::std::max(-32768., in.y()));
         out.y = XDoubleToFixed(y);
      }

      XRenderer::OutPoint XRenderer::makePointSafe(const Point& pt) const
      throws()
      {
         OutPoint xp;
         makePointSafe(pt, xp);
         return xp;
      }

      void XRenderer::makePointSafe(const Point& in, OutPoint& out) const
      throws()
      {
         out.x = XDoubleToFixed(in.x());
         out.y = XDoubleToFixed(in.y());
      }

      void XRenderer::fillTriangles(const OutPoint* pts, size_t n)
      throws()
      {
         _triangles.clear();
         for (const OutPoint* end = pts +  n; pts < end;) {
            XTriangle t;
            t.p1 = *pts++;
            t.p2 = *pts++;
            t.p3 = *pts++;
            _triangles.push_back(t);
         }
         XRenderCompositeTriangles(_ctx->display(), PictOpOver, _brush(), _dest, 0, 0, 0,// no offset
               &_triangles[0], _triangles.size());
      }

      void XRenderer::fillTriStrip(OutPoint* pts, size_t n)
      throws()
      {
         XRenderCompositeTriStrip(_ctx->display(), PictOpOver, _brush(), _dest, 0, 0, 0, pts, n);
      }

      void XRenderer::fillTriFan(OutPoint* pts, size_t n)
      throws()
      {
         XRenderCompositeTriFan(_ctx->display(), PictOpOver, _brush(), _dest, 0, 0, 0, pts, n);
      }

      void XRenderer::fillConvexPolygon(OutPoint* pts, size_t n)
      throws()
      {
         //FIXME: I think a convex polygon can be interpreted as a triangle fan
         XRenderCompositeTriFan(_ctx->display(), PictOpOver, _brush(), _dest, 0, 0, 0, pts, n);
      }

      void XRenderer::fillPolygon(OutPoint* pts, size_t n)
      throws()
      {
         // convert the polygon into an online-tri strip
         if (n <= 3) {
            if (n == 3) {
               fillTriangles(pts, 1);
            }
            return;
         }
         if (_brush.visibility() == Brush::OPAQUE) {
            convertToXPoints(pts, n, _xpoints);
            _legacy.fillPolygon(&_xpoints[0], n);
            return;
         }

         // tesselate the polygon
         const OutPoint* end = pts + n;

         // register the callback

         _gluTess._glPoints.clear();
         while (pts < end) {
            GLPoint v;
            v.vertex[0] = pts->x;
            v.vertex[1] = pts->y;
            v.vertex[2] = 0.0;

            _gluTess._glPoints.push_back(v);
            ++pts;
         }

         gluTessBeginPolygon(_gluTess._tesselator, this);
         gluTessBeginContour(_gluTess._tesselator);
         for (::std::vector< GLPoint>::iterator i = _gluTess._glPoints.begin(), iend = _gluTess._glPoints.end(); i
               != iend; ++i) {
            gluTessVertex(_gluTess._tesselator, i->vertex, i->vertex);
         }
         gluTessEndContour(_gluTess._tesselator);
         gluTessEndPolygon(_gluTess._tesselator);
      }

      void XRenderer::drawPolygon(const OutPoint* pts, size_t n)
      throws()
      {
         drawPolyline(pts, n, true);
      }

      void XRenderer::fillRectangle(const OutPoint& ul, const OutPoint& lr)
      throws()
      {
         OutPoint pts[4];
         pts[0] = ul;
         pts[1].x = lr.x;
         pts[1].y = ul.y;
         pts[2] = lr;
         pts[3].x = ul.x;
         pts[3].y = lr.y;

         XRenderCompositeTriFan(_ctx->display(), PictOpOver, _brush(), _dest, 0, 0, 0, pts, 4);
      }

      void XRenderer::drawRectangle(const OutPoint& tl, const OutPoint& br)
      throws()
      {
         ::XPoint ul = convertToXPoint(tl);
         ::XPoint lr = convertToXPoint(br);
         _legacy.drawRectangle(ul, lr);
      }

      void XRenderer::drawPoints(const OutPoint* pts, size_t n)
      throws()
      {
         convertToXPoints(pts, n, _xpoints);
         _legacy.drawPoints(&_xpoints[0], n);
      }

      void XRenderer::drawPolyline(const OutPoint* pts, size_t n, bool close)
      throws()
      {
         if (n < 2) {
            drawPoints(pts, n);
            return;
         }
         double lineWidth = _pen.width();
         // if we're not using any sort of anti-aliasing, then we can fallback onto simple X for drawing lines
         // which should be faster.
         if ((_pen.aaMode()==AntiAliasingMode::NONE) && (_pen.visibility() == Pen::OPAQUE) && (lineWidth == 1)) {
            convertToXPoints(pts, n, _xpoints);
            _legacy.drawPolyline(&_xpoints[0], n, close);
            return;
         }

         XFixed w = XDoubleToFixed(lineWidth);
         Alpha& mask = (_pen.aaMode()==AntiAliasingMode::NONE) ? _maskAANone : _mask;

         const OutPoint* pEnd = pts + n;
         const OutPoint* p = pts + 1;

         const size_t nTraps = 128;
         XTrap traps[nTraps + 5]; // allows us to close the polyline with 3 pieces

         OutPoint ul, lr;
         ul = lr = pts[0];

         mask.setAlpha(0);
         size_t i = 0;
         if (close) {
            i += makeLine(&pts[n] - 1, pts, w, traps, ul, lr);
         }
         while (true) {
            i += makeLine(pts, p, w, traps + i, ul, lr);
            pts = p++;
            if (i >= nTraps || p == pEnd) {
               if (i > 0) {
                  XRenderAddTraps(_ctx->display(), mask(), 0, 0, traps, i);
               }
               if (p == pEnd) {
                  break;
               }
               i = 0;
            }
         }

         double left = ::std::max(0.0, XFixedToDouble(ul.x) - lineWidth);
         double right = ::std::min((double) _width, XFixedToDouble(lr.x) + lineWidth);
         double top = ::std::max(0.0, XFixedToDouble(ul.y) - lineWidth);
         double bottom = ::std::min((double) _height, XFixedToDouble(lr.y) + lineWidth);

         // now, we composite the image
         XRenderComposite(_ctx->display(), PictOpOver, _pen(), mask(), _dest, left, top, left, top, left, top, right - left,
               bottom - top);

         // clear the alpha pict again
         XRenderColor c;
         c.red = c.blue = c.green = c.alpha = 0;
         XRenderFillRectangle(_ctx->display(), PictOpSrc, mask(), &c, left, top, right - left, bottom - top);

      }

      void XRenderer::renderImage(const ::timber::media::ImageBuffer& image, const OutPoint& tl)
      throws()
      {
         AffineTransform2D t(0, 1, XFixedToDouble(tl.x), XFixedToDouble(tl.y));
         renderImage(image, t);
      }

      void XRenderer::renderImage(const ::timber::media::ImageBuffer& image, const AffineTransform2D& T)
      throws()
      {
         // if the image is not being rotated or scaled, then we use the legacy
         // renderer, which is going to be faster
         if (_mask1x1.visibility() == Alpha::OPAQUE && T(0, 0) == T(1, 1) && T(0, 0) == 1 && T(0, 1) == T(1, 0) && T(0,
                     1) == 0) {
            _legacy.renderImage(image, T);
            return;
         }

         try {
            // XRender actually needs the inverse of T, which is the transform from destination to source
            // this is for performance reasons
            AffineTransform2D TX = T.inverse();

            // create the image for the buffered image
            //FIXME: really, we should cache this sometimes
            Visual* visual = DefaultVisual(_ctx->display(), DefaultScreen(_ctx->display()));
            _ximage = createX11Image(image, _ctx->display(), visual, _destFormat->depth, move(_ximage));
            if (_ximage.get() != 0) {
               // do we need to reallocate the drawable?
               if (_ximage->width() > _imageWidth || _ximage->height() > _imageHeight) {
                  _imageWidth = ::std::max(_imageWidth, (size_t) _ximage->width());
                  _imageHeight = ::std::max(_imageHeight, (size_t) _ximage->height());

                  XFreeGC(_ctx->display(), _imageGC);
                  XFreePixmap(_ctx->display(), _imageDrawable);
                  XRenderFreePicture(_ctx->display(), _image);

                  //LogEntry(logger()).info() << "Creating an image pixmap " << _imageWidth << "x" << _imageHeight << "x" << _srcFormat->depth << doLog;

                  _imageDrawable = XCreatePixmap(_ctx->display(), DefaultRootWindow(_ctx->display()), (unsigned int) _imageWidth,
                        (unsigned int) _imageHeight, _destFormat->depth);
                  _image = XRenderCreatePicture(_ctx->display(), _imageDrawable, _destFormat, 0, 0);
                  _imageGC = ::XCreateGC(_ctx->display(), _imageDrawable, 0, 0);
               }

               ::XPutImage(_ctx->display(), _imageDrawable, _imageGC, _ximage->x11Image(), // image
                     0, 0, // src pos
                     0, 0, // dest pos
                     _ximage->width(), _ximage->height());

               // transform the 4 vertices of the image

               size_t x, y, w, h;
               {
                  Point pt = T.transformPoint(0, 0);
                  double minx = pt.x();
                  double miny = pt.y();
                  double maxx = minx;
                  double maxy = miny;

                  pt = T.transformPoint(0, _ximage->height());
                  minx = ::std::min(minx, pt.x());
                  miny = ::std::min(miny, pt.y());
                  maxx = ::std::max(maxx, pt.x());
                  maxy = ::std::max(maxy, pt.y());
                  pt = T.transformPoint(_ximage->width(), _ximage->height());
                  minx = ::std::min(minx, pt.x());
                  miny = ::std::min(miny, pt.y());
                  maxx = ::std::max(maxx, pt.x());
                  maxy = ::std::max(maxy, pt.y());
                  pt = T.transformPoint(_ximage->width(), 0);
                  minx = ::std::min(minx, pt.x());
                  miny = ::std::min(miny, pt.y());
                  maxx = ::std::max(maxx, pt.x());
                  maxy = ::std::max(maxy, pt.y());

                  x = (size_t) ::std::max(0.0, minx - 1);
                  y = (size_t) ::std::max(0.0, miny - 1);
                  w = (size_t) ::std::min((double) _width, 1 + maxx - x);
                  h = (size_t) ::std::min((double) _height, 1 + maxy - y);
               }

               TX = AffineTransform2D(TX, AffineTransform2D(0, 1, (double) x, (double) y));

               XTransform xTransform;
               for (int i = 0; i < 2; ++i) {
                  for (int j = 0; j < 3; ++j) {
                     xTransform.matrix[i][j] = XDoubleToFixed(TX(i, j));
                  }
               }
               xTransform.matrix[2][0] = xTransform.matrix[2][1] = 0;
               xTransform.matrix[2][2] = XDoubleToFixed(1);

               XRenderSetPictureTransform(_ctx->display(), _image, &xTransform);

               // now, we composite the image
               XRenderComposite(_ctx->display(), PictOpOver, _image, _mask1x1.visibility() == Alpha::OPAQUE ? None
                     : _mask1x1(), _dest, 0, 0, 0, 0, x, y, w, h);
            }

         }
         catch (const ::std::exception& e) {
            logger().caught("Could not render image", e);
         }
      }

      void XRenderer::addVertex(GLdouble* v, XRenderer* data)
      {
         XRenderer::OutPoint x;
         x.x = static_cast< XFixed> (v[0]);
         x.y = static_cast< XFixed> (v[1]);
         data->_gluTess._triPoints.push_back(x);

      }

      void XRenderer::beginObject(GLenum type, XRenderer* data)
      {
         data->_gluTess._triPoints.clear();
         data->_gluTess._type = type;
      }

      void XRenderer::endObject(XRenderer* renderer)
      {
         OutPoint* pts = &renderer->_gluTess._triPoints[0];
         size_t n = renderer->_gluTess._triPoints.size();

         switch (renderer->_gluTess._type) {
            case GL_TRIANGLE_FAN:
               renderer->fillTriFan(pts, n);
               break;
            case GL_TRIANGLE_STRIP:
               renderer->fillTriStrip(pts, n);
               break;
            case GL_TRIANGLES:
               renderer->fillTriangles(pts, n / 3);
               break;
            case GL_LINE_LOOP:
               logger().warn("LineLoop found; not rendered");
               // ignore for now;
               break;
         };
      }

      void XRenderer::setFont(const ::timber::Pointer< ::indigo::Font>& font)
      {
         Pointer< XRenderFont> f = font.tryDynamicCast< XRenderFont> ();
         if (f && f->xftFont()) {
            _fontPen.setFont(f);
         }
         else {
            _fontPen.setFont(Pointer< XRenderFont> ());
            _legacy.setFont(font);
         }
      }

      void XRenderer::drawText(const ::std::string& str, const OutPoint& o)
      {
         ::XPoint xpt = convertToXPoint(o);
         if (!_fontPen.font()) {
            _legacy.drawText(str, xpt);
            return;
         }

         // check if the color has to be set for the font
         XftChar8* txt = const_cast< XftChar8*> (reinterpret_cast< const XftChar8*> (str.c_str()));

         XftDrawStringUtf8(_fontPen(), _fontPen.fontColor(), _fontPen.font()->xftFont(), xpt.x, xpt.y, txt,
               str.length());
      }

      Color XRenderer::readPixel(const OutPoint& pt)
      {
         ::XPoint xpt = convertToXPoint(pt);
         return _legacy.readPixel(xpt);
      }

      void XRenderer::setClipRectangle(const OutPoint& ul, const OutPoint& lr)
throws   ()
   {
      ::XPoint xul = convertToXPoint(ul);
      ::XPoint xlr = convertToXPoint(lr);
      _legacy.setClipRectangle(xul, xlr);

      ::XRectangle rect;
      rect.x = xul.x;
      rect.y = xul.y;
      rect.width = 1 + xlr.x - xul.x;
      rect.height = 1 + xlr.y - xul.y;

      XRenderSetPictureClipRectangles(_ctx->display(), _dest, 0, 0, &rect, 1);
   }

}

}
