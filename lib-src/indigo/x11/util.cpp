#include <indigo/x11/util.h>
#include <indigo/x11/X11Font.h>
#include <indigo/x11/X11Image.h>
#include <timber/logging.h>
#include <cstring>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace indigo {
   namespace x11 {
      namespace {
         /** The name of a logger */
         static const char* LOGGER = "indigo.x11";

         /**
          * Find the shift that needs to be applied to a 16-bit value
          * so that the most-significant bit lines up with the most significant
          * bit of the mask
          */
         static UInt find32BitShift(UInt mask)
         {
            // simple for now
            UInt bit(0x80000000);
            UInt shift = 0;
            assert(mask != 0 && "Mask cannot be 0 to find a shift");
            while ((mask & (bit >> shift)) == 0) {
               ++shift;
            }
            return shift;
         }

         static volatile bool pixmapError = false;
         static int createPixmapErrorHandler(Display *dpy, XErrorEvent *error)
         {
            pixmapError = true;
            return 0;
         }
      }

      const char* INDIGO_X11_XRENDERER = "INDIGO_X11_XRENDERER";

      Pixmap createPixmap(Display *dpy, Drawable drawable, ::std::uint32_t width, ::std::uint32_t height,
            ::std::uint32_t depth) throws()
      {
         /* synchronise to avoid possible previous error */
         XSync(dpy, False);

         /* initialize the error flag */
         pixmapError = false;

         /* install our error handler */
         auto oldHandler = XSetErrorHandler(createPixmapErrorHandler);

         /* attempt to create pixmap */
         Pixmap thePixmap = XCreatePixmap(dpy, drawable, width, height, depth);

         /* wait to see if server allocated pixmap */
         XSync(dpy, False);

         /* restore previous error handler */
         XSetErrorHandler(oldHandler);

         return pixmapError ? (Pixmap) 0 : thePixmap;
      }

      void logXExtensions(Display* display)
      throws()
      {
         int nExt = 0;
         char** ext = XListExtensions(display, &nExt);
         if (nExt == 0) {
            Log(LOGGER).info("No X11 extensions found");
         }
         else {
            LogEntry e(LOGGER);
            e.info() << "X11 Extensions : ";
            for (int i = 0; i < nExt; ++i) {
               e << ' ' << ext[i];
            }
            e << doLog;
         }
         XFreeExtensionList(ext);
      }

      bool isXRenderEnabled (Display* display) throws()
      {
         bool xrenderAvailable = false;
         {
            static bool alreadyLogged=false;

            const char* useExtension = ::std::getenv(INDIGO_X11_XRENDERER);
            if (useExtension==0 || ::std::strcmp("true",useExtension)==0) {
               int majorOpCode, firstEvent, firstError;
               xrenderAvailable = XQueryExtension(display, "RENDER", &majorOpCode, &firstEvent, &firstError);
               if (!alreadyLogged) {
                  alreadyLogged=true;

                  if (xrenderAvailable) {
                     if (useExtension) {
                        LogEntry(LOGGER).info() << "XRender extension enabled via environment variable " << INDIGO_X11_XRENDERER<< doLog;
                     }
                     else {
                        LogEntry(LOGGER).info() << "XRender extension enabled" << doLog;
                     }
                  }
                  else {
                     LogEntry(LOGGER).info() << "XRender extension not available" << doLog;
                  }
               }
            }
            else {
               if (!alreadyLogged) {
                  alreadyLogged=true;
                  LogEntry(LOGGER).info() << "XRender extension disabled via through environment variable " << INDIGO_X11_XRENDERER << doLog;
               }
            }
         }

         if (!xrenderAvailable) {
            static bool queryExtensions = true;
            if (queryExtensions) {
               queryExtensions = false;
               logXExtensions(display);
            }
         }
         return xrenderAvailable;
      }

      unique_ptr< X11Image> createX11Image(const ::timber::media::ImageBuffer& src, Display* dpy, Visual* vis,
            size_t depth, ::std::unique_ptr< X11Image> imagePtr)
      throws()
      {
         const UInt w = ::canopy::checked_cast< UInt>(src.width());
         const UInt h = ::canopy::checked_cast< UInt>(src.height());

         if (true || imagePtr.get() == 0 || imagePtr->width() > w || imagePtr->height() > h) {
            imagePtr = X11Image::create(dpy, vis, depth, w, h);
         }

         XImage* image = imagePtr->x11Image();

         const UInt redShift = find32BitShift(image->red_mask);
         const UInt greenShift = find32BitShift(image->green_mask);
         const UInt blueShift = find32BitShift(image->blue_mask);

         ::timber::media::ImageBuffer::PixelColor rgb;
         for (UInt y = 0; y < h; ++y) {
            for (UInt x = 0; x < w; ++x) {
               // need to encode the data
               src.pixel(x, y, rgb);
               UInt pixel = 0;
               // now, shift the value in the range for the each mask
               // first, though, shift the rgb intensities into the full integer range
               pixel |= ((UInt(rgb._red) * 65537) >> redShift) & image->red_mask;
               pixel |= ((UInt(rgb._green) * 65537) >> greenShift) & image->green_mask;
               pixel |= ((UInt(rgb._blue) * 65537) >> blueShift) & image->blue_mask;
               XPutPixel(image, x, y, pixel);
            }
         }
         return move(imagePtr);
      }

      unique_ptr< X11Image> createX11Image(const ::timber::media::ImageBuffer& src, Display* dpy,
            ::std::unique_ptr< X11Image> imagePtr)
throws   ()
   {
      int scr = XScreenNumberOfScreen(XDefaultScreenOfDisplay(dpy));
      Visual* visual = DefaultVisual(dpy, scr);
      int depth = DefaultDepth(dpy, scr);
      return createX11Image(src, dpy, visual, depth, ::std::move(imagePtr));
   }
}
}

