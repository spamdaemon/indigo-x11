#include <indigo/x11/X11ContextFactory.h>
#include <indigo/x11/X11RenderContext.h>
#include <indigo/x11/PixmapRenderContext.h>
#include <indigo/x11/SimpleRenderer.h>
#include <indigo/x11/PickRenderer.h>
#include <indigo/x11/XRenderer.h>
#include <indigo/x11/XRenderFont.h>
#include <indigo/x11/X11Font.h>
#include <indigo/x11/util.h>
#include <indigo/render/AbstractContext.h>
#include <timber/logging.h>

#include <memory>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;
using namespace ::timber::media;
using namespace ::tikal;
using namespace ::indigo::render;

#define DEBUG_LOGGING_ENABLED 0

namespace indigo {
   namespace x11 {
      namespace {

         template<class RENDERER>
         struct ContextImpl : public AbstractContext
         {

               /** The point type */
            private:
               typedef typename RENDERER::OutPoint RPoint;

               /** The points */
            private:
               typedef vector< RPoint> RPoints;
            public:

               ~ContextImpl()throws()
               {
               }

               // the maximum valid device coordinates are -16000 to 16000
               ContextImpl(unique_ptr< RENDERER> policy, double x, double y, double w, double h)
                     : AbstractContext(x, y, w, h, { -16000, 16000, -16000, 16000 }), _policy(move(policy))
               {
               }

               static bool equals(const RPoint& p, const RPoint& q)throws()
               {
                  return p.x == q.x && p.y == q.y;
               }

               void ensurePointSize(size_t n)throws()
               {
                  if (_points.size() < n) {
                     _points.resize(2 * n);
                  }
               }

               size_t makePoints(size_t n, const Point* pts)throws()
               {
                  assert(n > 0);

                  ensurePointSize(n);
                  const Point* pEnd = pts + n;
                  const Point* p = pts;
                  auto i = _points.begin();

                  //  first point is always a direct copy
                  *i = _policy->makePoint(*p);
                  ++i;
                  ++p;

                  while (p != pEnd) {
                     *i = _policy->makePoint(*p);
                     if (!equals(*i, *(i - 1))) {
                        ++i;
                     }
                     ++p;
                  }
                  return i - _points.begin();
               }

               size_t makePointsNoCheckDuplicate(size_t n, const Point* pts)throws()
               {
                  assert(n > 0);

                  ensurePointSize(n);
                  const Point* pEnd = pts + n;
                  const Point* p = pts;
                  auto i = _points.begin();
                  while (p != pEnd) {
                     *i = _policy->makePoint(*p);
                     ++i;
                     ++p;
                  }
                  return i - _points.begin();
               }

               void drawImage(double x, double y, const ImageRef& image, int32_t w, int32_t h)throws()
               {
                  // render the image immediately without scaling
                  if (w < 0) {
                     w = image->width();
                  }
                  if (h < 0) {
                     h = image->height();
                  }
                  if (w == 0 || h == 0) {
                     return;
                  }
                  assert(w > 0);
                  assert(h > 0);

                  Point pt = transformPoint(x, y);
                  RPoint xp = _policy->makePoint(pt);

                  if (static_cast< size_t>(w) == image->width() && static_cast< size_t>(h) == image->height()) {

                     _policy->renderImage(*image, xp);
                  }
                  else {
                     AffineTransform2D t(0, static_cast< double>(w) / image->width(),
                           static_cast< double>(h) / image->height(), xp.x, xp.y);
                     _policy->renderImage(*image, t);
                  }
               }

               void drawImage(double x, double y, const ImageRef& image,
                     const AffineTransform2D& imageTransform)throws()
               {
                  if (imageTransform.isIdentityTransform()) {
                     drawImage(x, y, image, -1, -1);
                     return;
                  }

                  Point pt = transformPoint(x, y);
                  RPoint xp = _policy->makePoint(pt);

                  // strictly speaking, we don't need to multiply
                  AffineTransform2D t(0, 1, xp.x, xp.y);
                  t = AffineTransform2D(t, imageTransform);

                  _policy->renderImage(*image, t);

               }

               void setFont(const ::timber::Reference< Font>& xfont)throws()
               {
                  _policy->setFont(xfont);
               }

               void setAlpha(double xalpha)throws()
               {
                  AbstractContext::setAlpha(xalpha);
                  _policy->setAlpha(xalpha);
               }

               void setAntiAliasingMode(AntiAliasingMode xmode)throws()
               {
                  _policy->setAntiAliasingMode(xmode);
               }

               void setSolidFillColor(const Color& xfillColor)throws()
               {
                  _policy->setFillColor(xfillColor);
               }

               void setStrokeColor(const Color& xstrokeColor)throws()
               {
                  _policy->setStrokeColor(xstrokeColor);
               }
               void setStrokeWidth(double xwidth)throws()
               {
                  _policy->setStrokeWidth(xwidth);
               }

               void drawTextUtf8(double x, double y, const string& utf8)throws()
               {
                  const Point pt = transformPoint(x, y);
                  RPoint xp(_policy->makePoint(pt));
                  _policy->drawText(utf8, xp);

               }

               void paintTransformedPolyline(PaintMode mode, size_t n, const Point* pts, bool close)throws()
               {
                  const bool draw = mode == 0 || ((mode & Context::DRAW) != 0);
                  if (draw) {
                     ensurePointSize(n + 1);
                     size_t sz = makePoints(n, pts);
                     if (close) {
                        _points[sz] = _points[0];
                        ++sz;
                     }
                     _policy->drawPolyline(&_points[0], sz);

                  }
               }

               void paintTransformedPolygon(PaintMode mode, size_t n, const Point* pts, bool convex)throws()
               {
                  const bool draw = ((mode & Context::DRAW) != 0);
                  const bool fill = (mode == 0 || (mode & Context::FILL) != 0);

                  ensurePointSize(n + 1);
                  size_t sz = makePoints(n, pts);
                  // need to close the polyline, if we draw it

                  if (fill) {
                     if (convex) {
                        _policy->fillConvexPolygon(&_points[0], sz);
                     }
                     else {
                        _policy->fillPolygon(&_points[0], sz);
                     }
                  }

                  if (draw) {
                     _points[sz] = _points[0];
                     _policy->drawPolyline(&_points[0], sz + 1);
                  }

               }

               void paintTransformedPoints(PaintMode mode, size_t n, const Point* pts, double pointSize)throws()
               {
                  const bool draw = ((mode & Context::DRAW) != 0);
                  const bool fill = (mode == 0 || (mode & Context::FILL) != 0);

                  Int ptSz = pointSize == 0 ? 1 : pointSize;
                  Int offset = ptSz / 2;

                  size_t sz = makePoints(n, pts);
                  auto end = _points.begin() + sz;
                  for (auto i = _points.begin(); i != end; ++i) {
                     RPoint ul = _policy->offsetPoint(*i, -offset, -offset);
                     RPoint lr = _policy->offsetPoint(ul, ptSz - 1, ptSz - 1);
                     if (fill) {
                        _policy->fillRectangle(ul, lr);
                     }
                     if (draw) {
                        _policy->drawRectangle(ul, lr);
                     }
                  }
               }

               void paintTransformedTriangles(PaintMode mode, size_t n, const Point* pts)throws()
               {
                  const bool draw = ((mode & Context::DRAW) != 0);
                  const bool fill = (mode == 0 || (mode & Context::FILL) != 0);
                  size_t sz = makePointsNoCheckDuplicate(n, pts);

                  if (draw && fill) {
                     auto end = _points.begin() + sz - 2;
                     RPoint vtx[4];
                     for (auto i = _points.begin(); i < end;) {
                        vtx[0] = *(i++);
                        vtx[1] = *(i++);
                        vtx[2] = *(i++);
                        vtx[3] = vtx[0];
                        _policy->fillConvexPolygon(vtx, 1);
                        _policy->drawPolyline(vtx, 4);
                     }
                  }
                  else if (fill) {
                     _policy->fillTriangles(&_points[0], sz);
                  }
                  else if (draw) {
                     auto end = _points.begin() + sz - 2;
                     RPoint vtx[4];
                     for (auto i = _points.begin(); i < end;) {
                        vtx[0] = *(i++);
                        vtx[1] = *(i++);
                        vtx[2] = *(i++);
                        vtx[3] = vtx[0];
                        _policy->drawPolyline(vtx, 4);
                     }
                  }
               }
               void paintTransformedTriStrip(PaintMode mode, size_t n, const Point* pts)throws()
               {
                  const bool draw = ((mode & Context::DRAW) != 0);
                  const bool fill = (mode == 0 || (mode & Context::FILL) != 0);
                  if (fill && !draw) {
                     size_t sz = makePointsNoCheckDuplicate(n, pts);
                     _policy->fillTriStrip(&_points[0], sz);
                  }
                  else {
                     AbstractContext::paintTransformedTriStrip(mode, n, pts);
                  }

               }

               unique_ptr< RENDERER> _policy;

               RPoints _points;
               double _width, _height;
               double _x, _y;
         };

         template<class RENDERER>
         struct PickContextImpl : public ::indigo::render::PickContext
         {
               PickContextImpl(unique_ptr< ContextImpl< PickRenderer< RENDERER> > > ctx)
                     : _context(move(ctx))
               {
               }

               ~PickContextImpl()throws()
               {
               }

               Context& renderContext()throws()
               {
                  return *_context;
               }

               bool testAndReset()throws()
               {
                  return _context->_policy->checkPixel();
               }

            private:
               unique_ptr< ContextImpl< PickRenderer< RENDERER> > > _context;
         };

         struct RendererWrapper
         {
               typedef Context Result;

               template<class RENDERER>
               inline unique_ptr< Result> operator()(unique_ptr< RENDERER> r, double x, double y, double w,
                     double h) const throws()
               {
                  return unique_ptr< Result>(new ContextImpl< RENDERER>(move(r), x, y, w, h));
               }
         };

         struct PickWrapper
         {
               typedef PickContext Result;

               PickWrapper(size_t pickX, size_t pickY)
                     : _pt(pickX, pickY)
               {
               }

               template<class RENDERER>
               inline unique_ptr< Result> operator()(unique_ptr< RENDERER> r, double x, double y, double w,
                     double h) const throws()
               {
                  typename RENDERER::OutPoint pt = r->makePoint(_pt);
                  //r->setClipRectangle(pt,pt);
                  unique_ptr< PickRenderer< RENDERER> > p(new PickRenderer< RENDERER>(move(r), pt));
                  unique_ptr< ContextImpl< PickRenderer< RENDERER> > > c(
                        new ContextImpl< PickRenderer< RENDERER> >(move(p), x, y, w, h));
                  return unique_ptr< Result>(new PickContextImpl< RENDERER>(move(c)));
               }

               const Point _pt;
         };

      }

      X11ContextFactory::X11ContextFactory(const ::timber::Reference< X11RenderContext>& ctx, const Color& defColor)
      throws()
            : _renderContext(ctx), _defaultColor(defColor), _logger("indigo.x11.X11ContextFactory"), _renderExtension(
                  isXRenderEnabled(ctx->display()))
      {

#if DEBUG_LOGGING_ENABLED != 0
         _logger.setLevel(Level::DEBUGGING);
#endif
      }

      X11ContextFactory::~X11ContextFactory()
      throws()
      {
      }

      Reference< X11ContextFactory> X11ContextFactory::create(const ::timber::Reference< X11RenderContext>& ctx,
            const Color& defColor)
            throws()
      {
         return new X11ContextFactory(ctx, defColor);
      }

      void X11ContextFactory::setDefaultFont(const Pointer< X11Font>& font)
      throws()
      {
         _defaultFont = font;
      }

      Pointer< X11Font> X11ContextFactory::defaultFont() const throws()
      {
         return _defaultFont;
      }

      template<class T>
      unique_ptr< typename T::Result> X11ContextFactory::createContextImpl(const T& contextWrapper, double x, double y,
            double w, double h, const Point& ul, const Point& lr)
            throws ()
      {
         if (_renderExtension) {
            unique_ptr< XRenderer> R(new XRenderer(_renderContext, (size_t) w, (size_t) h));
            R->setFillColor(_defaultColor);
            R->setStrokeColor(_defaultColor);

            return contextWrapper(move(R), x, y, w, h);
         }
         else {
            unique_ptr< SimpleRenderer> R(new SimpleRenderer(_renderContext));
            R->setClipRectangle(R->makePoint(ul), R->makePoint(lr));
            R->setFillColor(_defaultColor);
            R->setStrokeColor(_defaultColor);
            return contextWrapper(move(R), x, y, w, h);
         }
      }

      unique_ptr< Context> X11ContextFactory::createContext(double x, double y, double w, double h)
      throws ()
      {
         RendererWrapper wrapper;
         Point ul = Point(x, y);
         Point lr = Point(x + w - 1, y + h - 1);
         return createContextImpl(wrapper, x, y, w, h, ul, lr);
      }
      unique_ptr< ::indigo::render::PickContext> X11ContextFactory::createPickContext(double x, double y, double w,
            double h, size_t pickX, size_t pickY)
            throws ()
      {
         PickWrapper wrapper(pickX, pickY);
         const Point pt(pickX, pickY);
         return createContextImpl(wrapper, x, y, w, h, pt, pt);
      }

   }
}
