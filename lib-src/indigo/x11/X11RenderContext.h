#ifndef _INDIGO_X11_X11RENDERCONTEXT_H
#define _INDIGO_X11_X11RENDERCONTEXT_H

#ifndef _XLIB_H_
#include <X11/Xlib.h>
#endif

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace indigo {
   namespace x11 {

      /**
       * This class provides a baseclass for representing a display, a drawable, a colormap, and graphics context.
       */
      class X11RenderContext : public ::timber::Counted
      {
            /**
             * Create a new X11Render policy.
             * @param dp the display
             * @param gc the graphic context
             * @param canvas the drawable
             * @param colormap a color map
             * @param w the width of the canvas
             * @param h the height of the canvas
             */
         public:
            X11RenderContext(::Display* dp, ::GC gc, ::Drawable canvas, ::Colormap colormap)throws();

            /** Destructor */
         public:
            ~X11RenderContext()throws();

            /**
             * Get the display pointer.
             * @return the display pointer
             */
         public:
            inline ::Display* display() const throws() {return _display;}

            /**
             * Get  the drawable which acts as the canvas.
             * @return the drawable or canvas
             */
         public:
            inline ::Drawable drawable() const throws() {return _canvas;}

            /**
             * Get the graphics context
             * @return the graphics context
             */
         public:
            inline ::GC gc() const throws() {return _gc;}

            /**
             * Get the colormap.
             * @return the colormap.
             */
         public:
            inline ::Colormap colors() const throws() {return _colors;}

            /** The display */
         private:
            ::Display* _display;

            /** The drawable canvas */
         private:
            ::Drawable _canvas;

            /** The graphics context */
         private:
            ::GC _gc;

            /** A colormap */
         private:
            ::Colormap _colors;

      };
   }
}

#endif
