#include <indigo/x11/X11Renderer.h>
#include <indigo/x11/X11RenderContext.h>
#include <indigo/x11/SimpleRenderer.h>
#include <indigo/x11/PickRenderer.h>
#include <indigo/x11/XRenderer.h>
#include <indigo/x11/XRenderFont.h>
#include <indigo/x11/X11Font.h>
#include <indigo/x11/util.h>
#include <indigo/GfxVisitor.h>
#include <indigo/nodes.h>
#include <timber/logging.h>

#include <tikal/Region2D.h>
#include <tikal/Contour2D.h>
#include <tikal/Polygon2D.h>
#include <tikal/TriangleStrip2D.h>
#include <tikal/Path2D.h>
#include <tikal/BoundingBox2D.h>

#include <cmath>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <cstdlib>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;
using namespace ::timber::media;
using namespace ::tikal;

#define DEBUG_LOGGING_ENABLED 0

namespace indigo {
   namespace x11 {
      namespace {

         struct RenderState
         {
               inline RenderState()throws()
                     : _solidFillPolygon(false), _drawPolygon(false), _resetTransformMode(false), _opaque(true), _pickInfo(
                           0), _pickMode(false), _alpha(1), _aaMode(::indigo::AntiAliasingMode::FAST), _stroke(0), _solidFill(
                           0)
               // concatenate transforms by default
               {
                  _transform = _viewTransform;
               }

               inline bool isFillMode() const throws()
               {
                  return _solidFillPolygon;
               }
               inline bool isDrawMode() const throws()
               {
                  return _drawPolygon;
               }
               inline bool isResetTransform() const throws()
               {
                  return _resetTransformMode;
               }

               AffineTransform2D _viewTransform;
               AffineTransform2D _transform;
               bool _solidFillPolygon;
               bool _drawPolygon;
               bool _resetTransformMode;
               bool _opaque;
               const ::indigo::PickInfo* _pickInfo;
               bool _pickMode;
               double _alpha;
               ::indigo::AntiAliasingMode _aaMode;
               const StrokeNode* _stroke;
               const SolidFillNode* _solidFill;
         };
      }

      template<class RENDERER>
      struct X11Renderer::Visitor : public GfxVisitor
      {

            /** The point type */
         private:
            typedef typename RENDERER::OutPoint RPoint;

            /** The points */
         private:
            typedef ::std::vector< RPoint> RPoints;
         public:

            ~Visitor()throws()
            {
            }
            Visitor(RENDERER& policy, X11Renderer& r, double x, double y, double w, double h)
                  : _policy(policy), _renderer(r), _rc(r._renderContext), _width(w), _height(h), _x(x), _y(y)
            {
            }

            /**
             * Update the renderer's graphic state.
             */
            void updateGraphicState()
            {
               _state._solidFillPolygon = false;
               _state._drawPolygon = false;

               _policy.setAntiAliasingMode(_state._aaMode);
               if (_state._solidFill) {
                  _state._solidFillPolygon = true;
                  _policy.setFillColor(_state._solidFill->color());
               }
               if (_state._stroke) {
                  _state._drawPolygon = true;
                  _policy.setStrokeColor(_state._stroke->color());
                  _policy.setStrokeWidth(_state._stroke->width());
               }
               _policy.setAlpha(_state._alpha);
               if (_state._alpha == 0) {
                  _state._solidFillPolygon = _state._drawPolygon = false;
               }
               if (_state._alpha == 0) {
                  _state._solidFillPolygon = _state._drawPolygon = false;
               }

            }

            /**
             * Create a clip region for the current bounding box of the visible region
             * The clip region will be based on bounding box that is 1 pixel
             * larger.
             */
            Reference< Region2D> createClipRegion()
            {
               const AffineTransform2D inv = _state._transform.inverse();
               Point pt;
               // make the bounds just 1 pixel wider to eliminate clipping issues
               // at the boundaries
               pt = inv.transformPoint(_x - 1, _y - 1);
               BoundingBox2D bbox(pt.x(), pt.y());
               pt = inv.transformPoint(_x + _width + 1, _y - 1);
               bbox.insert(pt.x(), pt.y());
               pt = inv.transformPoint(_x + _width + 1, _y + _height + 1);
               bbox.insert(pt.x(), pt.y());
               pt = inv.transformPoint(_x - 1, _y + _width + 1);
               bbox.insert(pt.x(), pt.y());
               return Polygon2D::create(bbox);
            }

            void generateEllipsePoints(const Point& c, double M, double m, double inclination)
            {
               // for now, we'll just generate 36 points
               _points.clear();
               _points.reserve(37);

               const double PI2 = 2 * 3.14159265358979323846;
               const double di = PI2 / 36;

               const double cosine = ::std::cos(inclination);
               const double sine = ::std::sin(inclination);

               for (double i = 0; i < PI2; i += di) {
                  const double x0 = ::std::cos(i) * M;
                  const double y0 = ::std::sin(i) * m;

                  // rotate by inclination
                  const double x = cosine * x0 - sine * y0;
                  const double y = sine * x0 + cosine * y0;

                  Point pt = _state._transform.transformPoint(c.x() + x, c.y() + y);
                  RPoint xp(_policy.makePoint(pt));
                  _points.push_back(xp);
               }
               _points.push_back(RPoint());
               _points[_points.size() - 1] = _points[0];
            }

            inline Display* display()throws()
            {
               return _rc->display();
            }
            inline GC gc()throws()
            {
               return _rc->gc();
            }
            inline Drawable canvas()throws()
            {
               return _rc->drawable();
            }
            inline Colormap colors()throws()
            {
               return _rc->colors();
            }

            /**
             * A node is ignorable, if the mode is a pickmode and no pickinfo is set
             */
            inline bool isIgnorableNode()
            {
               return _state._pickMode && _state._pickInfo == 0;
            }

            void visit(const Coordinates2D&)throws()
            {
            }

            void visit(const CachableNode& cn)throws()
            {
               Pointer< Node> n = cn.node();
               if (!n) {
                  return;
               }
               if (cn.isSeparator()) {
                  RenderState state(_state);
                  n->accept(*this);
                  _state = state;
               }
               else {
                  n->accept(*this);
               }
            }

            void visit(const Texture& g)throws()
            {
            }

            void visit(const Icon& g)throws()
            {
               if (isIgnorableNode()) {
                  return;
               }

               if (g.image() == nullptr) {
                  return;
               }
               Point pt = _state._transform.transformPoint(0, 0);

               // strictly speaking, we don't need to multiply
               AffineTransform2D t(0, 1, pt.x(), pt.y());
               t = AffineTransform2D(t, g.transform());

               _policy.renderImage(*g.image(), t);

               if (_policy.checkPixel()) {
                  _renderer._pickInfo->info = _state._pickInfo;
                  _renderer._pickInfo->picked = &g;
                  _renderer._pickInfo->detail = 0;
               }
            }

            void visit(const Raster& g)throws()
            {
               if (isIgnorableNode()) {
                  return;
               }

               if (g.image() == nullptr) {
                  return;
               }

               // first, we need to transform the image region
               const Point xtl = g.topLeft();
               const Point xtr = g.topRight();
               const Point xbl = g.bottomLeft();
               const Point tl = _state._transform.transformPoint(xtl);
               const Point tr = _state._transform.transformPoint(xtr);
               const Point bl = _state._transform.transformPoint(xbl);

               double vec[2] { tr.x() - tl.x(), tr.y() - tl.y() };
               const double phi = ::std::atan2(vec[1], vec[0]);

               // need to determine the scale factor
               const double sx = ::std::sqrt(vec[0] * vec[0] + vec[1] * vec[1]) / (xtl.distanceTo(xtr));
               const double sy = bl.distanceTo(tl) / xtl.distanceTo(xbl);

               AffineTransform2D t(0, 1, tl.x(), tl.y());
               const AffineTransform2D imageTransform(phi, sx, sy, 0, 0);
               t = AffineTransform2D(t, imageTransform);

               _policy.renderImage(*g.image(), t);

               if (_policy.checkPixel()) {
                  _renderer._pickInfo->info = _state._pickInfo;
                  _renderer._pickInfo->picked = &g;
                  _renderer._pickInfo->detail = 0;
               }
            }

            void visit(const Coordinates3D& g)throws()
            {
            }

            void visit(const ::indigo::PickInfo& p)throws()
            {
               if (_state._pickMode) {
                  _state._pickInfo = &p;
               }
            }

            void visit(const AntiAliasingModeNode& g)throws()
            {
               _state._aaMode = g.antiAliasingMode();
               _policy.setAntiAliasingMode(_state._aaMode);
            }

            void visit(const ::indigo::AlphaNode& p)throws()
            {
               _state._alpha *= p.alpha();
               _policy.setAlpha(_state._alpha);
            }

            void visit(const TransformMode& g)throws()
            {
               switch (g.mode()) {
                  case TransformMode::RESET:
                     _state._resetTransformMode = true;
                     break;
                  case TransformMode::CONCATENATE:
                  default:
                     _state._resetTransformMode = false;
               }
            }

            void visit(const Text& txt)throws()
            {
               if (isIgnorableNode()) {
                  return;
               }
               if (txt.length() == 0) {
                  return;
               }
               Pointer< ::indigo::Font> ft = txt.font();
               if (!ft) {
                  ft = _renderer.defaultFont();
                  if (!ft) {
                     return;
                  }
               }

               updateGraphicState();

               static const Point ORIGIN(0, 0, 0);
               const Point pt = _state._transform.transformPoint(ORIGIN);
               RPoint xp(_policy.makePoint(pt));

               _policy.setFont(ft);
               _policy.drawText(txt.text(), xp);

               if (_policy.checkPixel()) {
                  _renderer._pickInfo->info = _state._pickInfo;
                  _renderer._pickInfo->picked = &txt;
                  _renderer._pickInfo->detail = 0;
               }
            }

            void visit(const ViewTransform2D& t)throws()
            {
               const AffineTransform2D inv(_state._viewTransform.inverse());

               // premultiply the view transform by the compound object transform, which contains a view transform
               const AffineTransform2D objTransform = AffineTransform2D(inv, _state._transform);

               // the new transform
               if (_state.isResetTransform()) {
                  _state._viewTransform = t.transform();
               }
               else {
                  _state._viewTransform = AffineTransform2D(_state._viewTransform, t.transform());
               }

               // update the overal transform
               _state._transform = AffineTransform2D(_state._viewTransform, objTransform);
            }

            void visit(const Transform2D& t)throws()
            {
               if (_state.isResetTransform()) {
                  // reset the transform
                  _state._transform = AffineTransform2D(_state._viewTransform, t.transform());
               }
               else {
                  // concatenate the transforms
                  _state._transform = AffineTransform2D(_state._transform, t.transform());
               }
            }

            void visit(const Circles& g)throws()
            {
               if (isIgnorableNode()) {
                  return;
               }
               size_t sz = g.count();
               if (sz > 0) {
                  // render from the back to the front!!
                  updateGraphicState();
                  if (!_state.isFillMode() && !_state.isDrawMode()) {
                     return;
                  }
                  while (sz-- > 0) {
                     Circle p(g.shape(sz));
                     if (p.radius() == 0) {
                        const Point pt = _state._transform.transformPoint(p.center());
                        const RPoint xp = _policy.makePointSafe(pt);
                        _policy.drawPoints(&xp, 1);
                     }
                     else {
                        generateEllipsePoints(p.center(), p.radius(), p.radius(), 0);
                        if (_state.isFillMode()) {
                           _policy.fillConvexPolygon(&_points[0], _points.size());
                        }
                        if (_state.isDrawMode()) {
                           _policy.drawPolyline(&_points[0], _points.size());
                        }
                     }

                     if (_policy.checkPixel()) {
                        _renderer._pickInfo->info = _state._pickInfo;
                        _renderer._pickInfo->picked = &g;
                        _renderer._pickInfo->detail = sz;
                     }
                  }
               }
            }

            void visit(const Ellipses& g)throws()
            {
               if (isIgnorableNode()) {
                  return;
               }
               size_t sz = g.count();
               if (sz > 0) {
                  // render from the back to the front!!
                  updateGraphicState();
                  if (!_state.isFillMode() && !_state.isDrawMode()) {
                     return;
                  }
                  while (sz-- > 0) {
                     Ellipse p(g.shape(sz));

                     double majorAxis = p.majorAxis();
                     double minorAxis = p.minorAxis();

                     const Point pt = _state._transform.transformPoint(p.center());

                     if (majorAxis == 0 && minorAxis == 0) {
                        const RPoint xp = _policy.makePointSafe(pt);
                        _policy.drawPoints(&xp, 1);
                     }
                     else {
                        generateEllipsePoints(p.center(), p.majorAxis(), p.minorAxis(), p.theta());
                        if (_state.isFillMode()) {
                           _policy.fillConvexPolygon(&_points[0], _points.size());
                        }
                        if (_state.isDrawMode()) {
                           _policy.drawPolyline(&_points[0], _points.size());
                        }
                     }

                     if (_policy.checkPixel()) {
                        _renderer._pickInfo->info = _state._pickInfo;
                        _renderer._pickInfo->picked = &g;
                        _renderer._pickInfo->detail = sz;
                     }
                  }
               }
            }

            void visit(const PolyLines& g)throws()
            {
               if (isIgnorableNode()) {
                  return;
               }
               size_t sz = g.count();
               if (sz > 0) {
                  updateGraphicState();
                  if (!_state.isDrawMode()) {
                     return;
                  }

                  // render from the back to the front!!
                  while (sz-- > 0) {
                     PolyLine p(g.shape(sz));
                     if (p.vertexCount() == 0) {
                        continue;
                     }
                     _points.clear();
                     _points.reserve(p.vertexCount() + 1);
                     RPoint xPrev;
                     for (size_t i = 0; i < p.vertexCount(); ++i) {
                        const Point pt = _state._transform.transformPoint(p.vertex(i));
                        RPoint xp(_policy.makePoint(pt));
                        if (_points.empty() || xPrev.x != xp.x || xPrev.y != xp.y) {
                           _points.push_back(xp);
                           xPrev = xp;
                        }
                     }
                     if (p.isClosed() && !_points.empty()) {
                        // careful, this works, because we have enough memory reserved
                        _points.push_back(_points[0]);
                     }
                     _policy.drawPolyline(&_points[0], _points.size());

                     if (_policy.checkPixel()) {
                        _renderer._pickInfo->info = _state._pickInfo;
                        _renderer._pickInfo->picked = &g;
                        _renderer._pickInfo->detail = sz;
                     }
                  }
               }
            }

            void visit(const Points& g)throws()
            {
               if (isIgnorableNode()) {
                  return;
               }
               size_t sz = g.count();
               if (sz > 0) {
                  updateGraphicState();
                  // render from the back to the front!!
                  Int ptSz = g.pointSize() == 0 ? 1 : g.pointSize();
                  Int offset = ptSz / 2;

                  while (sz-- > 0) {
                     Point p(g.shape(sz));
                     const Point pt(_state._transform.transformPoint(g.shape(sz)));
                     RPoint c = _policy.makePoint(pt);
                     RPoint ul = _policy.offsetPoint(c, -offset, -offset);
                     RPoint lr = _policy.offsetPoint(ul, ptSz - 1, ptSz - 1);

                     if (_state.isFillMode()) {
                        _policy.fillRectangle(ul, lr);
                     }
                     if (_state.isDrawMode()) {
                        _policy.drawRectangle(ul, lr);
                     }

                     if (_policy.checkPixel()) {
                        _renderer._pickInfo->info = _state._pickInfo;
                        _renderer._pickInfo->picked = &g;
                        _renderer._pickInfo->detail = sz;
                     }
                  }
               }
            }

            void visit(const NurbsCurves& g)throws()
            {
               if (isIgnorableNode()) {
                  return;
               }
               size_t sz = g.count();
               if (sz > 0) {
                  // render from the back to the front!!
                  updateGraphicState();
                  if (!_state.isDrawMode()) {
                     return;
                  }

                  while (sz-- > 0) {
                     const NURBS& c = g.shape(sz);
                     _points.clear();
                     size_t nPoints = 1 + c.controlPointCount() * 4;
                     _points.reserve(nPoints);
                     double u = c.lowerBound();
                     double v = c.upperBound();
                     double du = (v - u) / (nPoints - 1);
                     for (size_t i = 0; i < nPoints; ++i) {
                        const Point pt(_state._transform.transformPoint(c.pointAt(u)));
                        _points.push_back(_policy.makePoint(pt));
                        u += du;
                        if (u > v) {
                           u = v;
                        }
                     }
                     //fixme; should be able to draw a NURBS curve
                     _policy.drawPolyline(&_points[0], _points.size());

                     if (_policy.checkPixel()) {
                        _renderer._pickInfo->info = _state._pickInfo;
                        _renderer._pickInfo->picked = &g;
                        _renderer._pickInfo->detail = sz;
                     }
                  }
               }
            }

            void visit(const TriangleSet& g)throws()
            {
               if (isIgnorableNode()) {
                  return;
               }
               size_t sz = g.count();
               if (sz > 0) {
                  RPoint vtx[4];
                  // render from the back to the front!!
                  updateGraphicState();
                  int mode = 0;
                  if (_state.isFillMode()) {
                     mode |= 1;
                  }
                  if (_state.isDrawMode()) {
                     mode |= 2;
                  }
                  if (mode == 0) {
                     return;
                  }
                  while (sz-- > 0) {
                     Triangles p(g.shape(sz));
                     _points.clear();
                     _points.reserve(p.vertexCount());
                     for (size_t i = 0; i < p.vertexCount(); ++i) {
                        const Point pt = _state._transform.transformPoint(p.vertex(i));
                        RPoint xp(_policy.makePoint(pt));
                        _points.push_back(xp);
                     }

                     const size_t tCount = p.count();

                     if (p.type() == Triangles::TRIANGLES) {
                        switch (mode) {
                           case 3:
                              for (size_t i = 0; i < tCount; ++i) {
                                 ::std::copy(_points.begin() + i * 3, _points.begin() + i * 3 + 3, &vtx[0]);
                                 vtx[3] = vtx[0];
                                 _policy.fillTriangles(vtx, 3);
                                 _policy.drawPolyline(vtx, 4);
                              }
                              break;
                           case 1:
                              for (size_t i = 0; i < tCount; ++i) {
                                 _policy.fillTriangles(&_points[i * 3], 3);
                              }
                              break;
                           case 2:
                              for (size_t i = 0; i < tCount; ++i) {
                                 ::std::copy(_points.begin() + i * 3, _points.begin() + i * 3 + 3, &vtx[0]);
                                 vtx[3] = vtx[0];
                                 _policy.drawPolyline(vtx, 4);
                              }
                              break;
                        }
                     }
                     else if (p.type() == Triangles::STRIP) {
                        for (size_t i = 0; i < tCount; ++i) {
                           if ((i % 2) == 0) {
                              vtx[0] = _points[i];
                              vtx[1] = _points[i + 1];
                           }
                           else {
                              vtx[0] = _points[i + 1];
                              vtx[1] = _points[i];
                           }
                           vtx[2] = _points[i + 2];
                           switch (mode) {
                              case 1:
                                 _policy.fillTriangles(vtx, 3);
                              case 2:
                                 vtx[3] = vtx[0];
                                 _policy.drawPolyline(vtx, 4);
                              case 3:
                                 vtx[3] = vtx[0];
                                 _policy.fillTriangles(vtx, 3);
                                 _policy.drawPolyline(vtx, 4);
                           }
                        }
                     }
                     else {
                        vtx[0] = vtx[3] = _points[0];
                        switch (mode) {
                           case 1:
                              for (size_t i = 0; i < tCount; ++i) {
                                 vtx[1] = _points[i + 1];
                                 vtx[2] = _points[i + 2];
                                 _policy.fillTriangles(vtx, 3);
                              }
                              break;
                           case 2:
                              for (size_t i = 0; i < tCount; ++i) {
                                 vtx[1] = _points[i + 1];
                                 vtx[2] = _points[i + 2];
                                 _policy.drawPolyline(vtx, 4);
                              }
                              break;
                           case 3:
                              for (size_t i = 0; i < tCount; ++i) {
                                 vtx[1] = _points[i + 1];
                                 vtx[2] = _points[i + 2];
                                 _policy.fillTriangles(vtx, 3);
                                 _policy.drawPolyline(vtx, 4);
                              }
                              break;

                        }
                     }
                     if (_policy.checkPixel()) {
                        _renderer._pickInfo->info = _state._pickInfo;
                        _renderer._pickInfo->picked = &g;
                        _renderer._pickInfo->detail = sz;
                     }
                  }
               }
            }

            void fillTriangleStrips(Region2D& strips)
            {
               if (isIgnorableNode()) {
                  return;
               }
               for (size_t i = 0, sz = strips.areaCount(); i < sz; ++i) {
                  Pointer< TriangleStrip2D> r = strips.area(i).tryDynamicCast< TriangleStrip2D>();
                  if (!r) {
                     _renderer._logger.bug("To triangle strips did not create a triangle strip");
                     continue;
                  }
                  const vector< Point2D>& vtx = r->stripVertices();

                  _points.clear();
                  _points.reserve(vtx.size());
                  for (vector< Point2D>::const_iterator j = vtx.begin(); j != vtx.end(); ++j) {
                     const Point pt = _state._transform.transformPoint(j->x(), j->y());
                     const RPoint xp(_policy.makePointSafe(pt));
                     _points.push_back(xp);
                  }
                  _policy.fillTriStrip(&_points[0], _points.size());
               }
            }

            /**
             * Render a contour consisting of possibly many.
             * @param r a region
             * @param doFill if true, the fill the region
             * @param doDraw if true, then draw the region
             */
            bool renderPolygon(Polygon2D& r, bool doFill, bool doDraw)
            {
               // create a tri-strip
               if (!_state._opaque && doFill) {
                  Pointer< Region2D> strips = r.toTriangleStrips();
                  if (strips) {
                     fillTriangleStrips(*strips);
                     doFill = false;
                     if (!doDraw) {
                        return true;
                     }
                  }
               }

               _points.clear();
               _points.reserve(r.vertexCount() + 2);

               RPoint xPrev;
               for (size_t i = 0, sz = r.vertexCount(); i != sz; ++i) {
                  Point2D v = r.vertex(i);
                  const Point pt = _state._transform.transformPoint(v.x(), v.y());
                  RPoint xp(_policy.makePointSafe(pt));
                  if (_points.empty() || xPrev.x != xp.x || xPrev.y != xp.y) {
                     _points.push_back(xp);
                     xPrev = xp;
                  }
               }

               // cannot render something with less than 3 vertices
               if (_points.size() < 3) {
                  return false;
               }

               _points.push_back(_points[0]);

               if (doFill) {
                  _policy.fillPolygon(&_points[0], _points.size());
               }
               if (doDraw) {
                  // we've already reserved enough space!
                  // need to add an extra point to close the contour
                  _points.push_back(_points[0]);
                  _policy.drawPolygon(&_points[0], _points.size());
               }
               return true;
            }

            /**
             * Render a contour consisting of possibly many.
             * @param r a region
             * @param doFill if true, the fill the region
             * @param doDraw if true, then draw the region
             */
            bool renderContour(Contour2D& r, bool doFill, bool doDraw)
            {
               Polygon2D* polygon = dynamic_cast< Polygon2D*>(&r);
               if (polygon) {
                  return renderPolygon(*polygon, doFill, doDraw);
               }

               if (r.maxDegree() > 1) {
                  // ignore contours of degree > 1
                  return false;
               }

               _points.clear();
               _points.reserve(r.segmentCount() + 2);

               RPoint xPrev;
               Point2D pts[2];
               for (size_t i = 0; i < r.segmentCount(); ++i) {
                  r.segment(i, pts);
                  const Point pt = _state._transform.transformPoint(pts[0].x(), pts[0].y());
                  RPoint xp(_policy.makePointSafe(pt));
                  if (_points.empty() || xPrev.x != xp.x || xPrev.y != xp.y) {
                     _points.push_back(xp);
                     xPrev = xp;
                  }
               }
               // cannot render something with less than 3 vertices
               if (_points.size() < 3) {
                  return false;
               }

               if (doFill) {
                  _policy.fillPolygon(&_points[0], _points.size());
               }
               if (doDraw) {
                  // we've already reserved enough space!
                  // need to add an extra point to close the contour
                  _points.push_back(_points[0]);
                  _policy.drawPolygon(&_points[0], _points.size());
               }
               return true;
            }
            bool isVisible(const BoundingBox2D& bbox, const Pointer< Region2D>& curClip, Pointer< Region2D>& clip)
            {
               const Point tl = _state._transform.transformPoint(bbox.minx(), bbox.maxy());
               const Point tr = _state._transform.transformPoint(bbox.maxx(), bbox.maxy());
               const Point bl = _state._transform.transformPoint(bbox.minx(), bbox.miny());
               const Point br = _state._transform.transformPoint(bbox.maxx(), bbox.miny());

               // here we're in the screens coordinate frame!!!
               double minX = ::std::min(tl.x(), ::std::min(tr.x(), ::std::min(bl.x(), br.x())));
               double minY = ::std::min(tl.y(), ::std::min(tr.y(), ::std::min(bl.y(), br.y())));
               double maxX = ::std::max(tl.x(), ::std::max(tr.x(), ::std::max(bl.x(), br.x())));
               double maxY = ::std::max(tl.y(), ::std::max(tr.y(), ::std::max(bl.y(), br.y())));

               // this the clip against the visible region
               if (maxX < _x || minX > _x + _width || maxY < _y || minY > _y + _height) {
                  //	  _renderer._logger.info("Region not visible");
                  return false;
               }

               // here we basically ensure that we clip against the valid region for
               // X11 points, which are represented as signed shorts. if a points
               // falls outside this validity region, then it is clipped to the
               // visible region
               // FIXME: we chose 16 arbitrarly, because we can represent the difference
               // between maxX-minX as a short; this should really be defined by the renderer
               if (maxX > 16000 || minX < -16000 || minY < -16000 || maxY > 16000) {
                  if (curClip) {
                     clip = curClip;
                  }
                  else {
                     clip = createClipRegion();
                  }
               }
               //	_renderer._logger.info("Region visible");
               return true;
            }

            /**
             * Render ab area consisting of possibly many contours or area, but
             * not containing hold if doFill == true
             * @param r a region
             * @param doFill if true, the fill the region
             * @param doDraw if true, then draw the region
             */
            void renderArea(Area2D& r, bool doFill, bool doDraw)
            {
               if (!renderContour(*r.outsideContour(), doFill, doDraw)) {
                  return;
               }
               if (doDraw) {
                  // internal contours are only rendered when drawing, not when filling
                  for (size_t i = 0, n = r.contourCount(); i < n; ++i) {
                     renderContour(*r.insideContour(i), false, true);
                  }
               }
            }

            void visit(const Regions& g)throws()
            {
               if (isIgnorableNode()) {
                  return;
               }
               size_t sz = g.count();
               if (sz > 0) {
                  Pointer< Region2D> clip;
                  // render from the back to the front!!
                  updateGraphicState();
                  if (!_state.isFillMode() && !_state.isDrawMode()) {
                     return;
                  }
                  while (sz-- > 0) {
                     Pointer< Region2D> reg(g.shape(sz));
                     if (!reg) {
                        continue;
                     }
                     // check if the region is visible at all
                     // or if it needs any clipping
                     Pointer< Region2D> currentClip;
                     // pass in the current clip region or 0 if it
                     // needs to be built
                     if (!isVisible(reg->bounds(), clip, currentClip)) {
                        continue;
                     }
                     // check if a clip is necessary
                     if (currentClip) {
                        clip = currentClip;
                        reg = clip->intersect(reg);
                        if (!reg) {
                           continue;
                        }
                     }

                     // when filling polygons, we need to remove holes for X11
                     Pointer< Region2D> noHoles = reg;
                     if (_state.isFillMode()) {
                        noHoles = reg->removeHoles();
                        if (!noHoles) {
                           _renderer._logger.warn("Could not remove holes from region; skipping region");
                           continue;
                        }
                     }

                     // if noHoles is the same region, then nothing needs to be done
                     // because the region has no holes
                     if (noHoles == reg) {
                        for (size_t j = 0, cnt = reg->areaCount(); j < cnt; ++j) {
                           renderArea(*reg->area(j), _state.isFillMode(), _state.isDrawMode());
                        }
                     }
                     else {
                        assert(_state.isFillMode());
                        // render filled area first
                        for (size_t j = 0, cnt = noHoles->areaCount(); j < cnt; ++j) {
                           renderArea(*noHoles->area(j), true, false);
                        }
                        // render the original area in draw mode
                        if (_state.isDrawMode()) {
                           for (size_t j = 0, cnt = reg->areaCount(); j < cnt; ++j) {
                              renderArea(*reg->area(j), false, true);
                           }
                        }
                     }
                     if (_policy.checkPixel()) {
                        _renderer._pickInfo->info = _state._pickInfo;
                        _renderer._pickInfo->picked = &g;
                        _renderer._pickInfo->detail = sz;
                     }
                  }
               }
            }

            void visit(const Paths& g)throws()
            {
               if (isIgnorableNode()) {
                  return;
               }
               size_t sz = g.count();
               if (sz > 0) {
                  updateGraphicState();
                  if (!_state.isDrawMode()) {
                     return;
                  }

                  Pointer< Region2D> clip;
                  RPoint xPrev;
                  // render from the back to the front!!
                  while (sz-- > 0) {
                     Pointer< Path2D> p(g.shape(sz));
                     if (!p) {
                        continue;
                     }
                     // check if the region is visible at all
                     // or if it needs any clipping
                     Pointer< Region2D> currentClip;
                     // pass in the current clip region or 0 if it
                     // needs to be built
                     if (!isVisible(p->bounds(), clip, currentClip)) {
                        continue;
                     }
                     if (currentClip) {
                        clip = currentClip;
                        p = clip->clipPath(p);
                        if (!p) {
                           continue;
                        }
                     }

                     if (p->maxDegree() > 1) {
                        // cannot render paths of higher degrees (yet)
                        continue;
                     }

                     Point2D pts; // a maximum 2 points due to maxdegree of 1
                     _points.clear();
                     _points.reserve(p->segmentCount() + 1);
                     for (size_t j = 0, cnt = p->segmentCount(); j < cnt; ++j) {
                        size_t deg = p->segment(j, &pts);
                        if (deg == 0) {
                           if (_points.size() > 0) {
                              _policy.drawPolyline(&_points[0], _points.size());
                           }
                           _points.clear();
                        }
                        const Point pt = _state._transform.transformPoint(pts.x(), pts.y());
                        RPoint xp(_policy.makePointSafe(pt));
                        if (_points.empty() || xPrev.x != xp.x || xPrev.y != xp.y) {
                           _points.push_back(xp);
                           xPrev = xp;
#if DEBUG_LOGGING_ENABLED != 0
                           LogEntry(_renderer._logger).debugging() << _points.size() << ": point " << pt.x() << ", " << pt.y() << " [ " << pts.x() << ' ' << pts.y() << " ] " << doLog;
#endif
                        }
                     }
                     if (_points.size() > 0) {
                        _policy.drawPolyline(&_points[0], _points.size());
                     }
                     if (_policy.checkPixel()) {
                        _renderer._pickInfo->info = _state._pickInfo;
                        _renderer._pickInfo->picked = &g;
                        _renderer._pickInfo->detail = sz;
                     }
                  }
               }
            }

            void visit(const Polygons& g)throws()
            {
               if (isIgnorableNode()) {
                  return;
               }
               size_t sz = g.count();
               if (sz > 0) {
                  // render from the back to the front!!
                  updateGraphicState();
                  if (!_state.isFillMode() && !_state.isDrawMode()) {
                     return;
                  }
                  while (sz-- > 0) {
                     Polygon p(g.shape(sz));
                     if (p.vertexCount() < 3) {
                        continue;
                     }
                     _points.clear();
                     _points.reserve(p.vertexCount() + 2);
                     RPoint xPrev;
                     for (size_t i = 0; i < p.vertexCount(); ++i) {
                        //FIXME: need to clip the polygon to the valid XRender or X11 point type!
                        const Point pt = _state._transform.transformPoint(p.vertex(i));
                        RPoint xp(_policy.makePoint(pt));
                        if (_points.empty() || xPrev.x != xp.x || xPrev.y != xp.y) {
                           _points.push_back(xp);
                           xPrev = xp;
                        }
                     }

                     if (_state.isFillMode()) {
                        if (p.shapeType() == Polygon::CONVEX_SHAPE) {
                           _policy.fillConvexPolygon(&_points[0], _points.size());
                        }
                        else {
                           _policy.fillPolygon(&_points[0], _points.size());
                        }
                     }
                     if (_state.isDrawMode()) {
                        // we've already reserved enough space!
                        _points.push_back(_points[0]);
                        _policy.drawPolyline(&_points[0], _points.size());
                     }
                     if (_policy.checkPixel()) {
                        _renderer._pickInfo->info = _state._pickInfo;
                        _renderer._pickInfo->picked = &g;
                        _renderer._pickInfo->detail = sz;
                     }
                  }
               }
            }

            void visit(const SolidFillNode& f)throws()
            {
               const SolidFillNode* fn(0);
               if (f.color().opacity() != 0) {
                  fn = &f;
               }
               if (_state._solidFill != fn) {
                  // change the fill
                  _state._solidFill = fn;
                  if (fn) {
                     _policy.setFillColor(_state._solidFill->color());
                  }
               }
            }

            void visit(const StrokeNode& f)throws()
            {
               const StrokeNode* fn(0);
               if (f.color().opacity() != 0) {
                  fn = &f;
               }
               if (_state._stroke != fn) {
                  // change the stroke
                  _state._stroke = fn;
                  if (fn) {
                     _policy.setStrokeColor(_state._stroke->color());
                     _policy.setStrokeWidth(_state._stroke->width());
                  }
               }
            }

            void visit(const Switch& s)throws()
            {
               size_t i = s.visibleChild();
               if (i == invalidIndex()) {
                  return;
               }
               if (i < s.nodeCount()) {
                  const Pointer< Node> h(s.getNode(i));
                  if (h) {
                     if (s.isSeparator()) {
                        RenderState state(_state);
                        h->accept(*this);
                        _state = state;
                     }
                     else {
                        h->accept(*this);
                     }
                  }
               }
            }

            void visitChildren(const CompositeNode& g)throws()
            {
               size_t sz = g.nodeCount();
               // check if there are any children; if not, then it's useless
               // to do other work.
               if (sz > 0) {
                  // render from the back to the front!!
                  for (size_t i = 0; i < sz; ++i) {
                     const Pointer< Node> h(g.getNode(i));
                     if (h) {
                        h->accept(*this);
                     }
                  }
               }
            }

            void visit(const Separator& s)throws()
            {
               Pointer< Node> c = s.node();
               if (c) {
                  RenderState state(_state);
                  c->accept(*this);
                  _state = state;
               }
            }

            void visit(const CompositeNode& g)throws()
            {
               if (g.nodeCount() > 0) {
                  if (g.isSeparator()) {
                     RenderState state(_state);
                     visitChildren(g);
                     _state = state;
                  }
                  else {
                     visitChildren(g);
                  }
               }
            }

            RENDERER& _policy;
            X11Renderer& _renderer;
            Reference< X11RenderContext> _rc;
            RPoints _points;
            RenderState _state;
            double _width, _height;
            double _x, _y;

      };

      X11Renderer::X11Renderer(const ::timber::Reference< X11RenderContext>& rc, const Color& defColor)
      throws()
            : _renderContext(rc), _defaultColor(defColor), _renderExtension(isXRenderEnabled(rc->display())), _logger(
                  "indigo.x11.X11Renderer")
      {

#if DEBUG_LOGGING_ENABLED != 0
         _logger.setLevel(Level::DEBUGGING);
#endif
      }

      X11Renderer::~X11Renderer()
      throws()
      {
      }

      void X11Renderer::enablePickMode(size_t x, size_t y)
      throws()
      {
         _pickInfo.reset(new PickInfo());
         _pickInfo->x = x;
         _pickInfo->y = y;
      }

      ::std::unique_ptr< X11Renderer::PickInfo> X11Renderer::clearPickMode()
      throws()
      {
         return move(_pickInfo);
      }

      Reference< X11Renderer> X11Renderer::create(const ::timber::Reference< X11RenderContext>& rc,
            const Color& defColor)
            throws()
      {
         return new X11Renderer(rc, defColor);
      }

      void X11Renderer::setDefaultFont(const Pointer< X11Font>& font)
      throws()
      {
         _defaultFont = font;
      }

      Pointer< X11Font> X11Renderer::defaultFont() const throws()
      {
         return _defaultFont;
      }

      void X11Renderer::destroy()
      throws()
      {
         delete this;
      }

      void X11Renderer::render(const Reference< Node>& g, double x, double y, double w, double h)
      throws ()
      {

         Point ul, lr;

         if (_pickInfo.get()) {
            ul = Point(_pickInfo->x, _pickInfo->y);
            lr = ul;
         }
         else {
            ul = Point(x, y);
            lr = Point(x + w - 1, y + h - 1);
         }
         if (_renderExtension) {
            unique_ptr< XRenderer> R(new XRenderer(_renderContext, (size_t) w, (size_t) h));
            R->setClipRectangle(R->makePoint(ul), R->makePoint(lr));
            if (_pickInfo.get()) {
               XRenderer::OutPoint pt = R->makePoint(Point(_pickInfo->x, _pickInfo->y));
               unique_ptr< PickRenderer< XRenderer> > P(new PickRenderer< XRenderer>(move(R), pt));
               P->setFillColor(_defaultColor);
               P->setStrokeColor(_defaultColor);
               Visitor< PickRenderer< XRenderer> > iter(*P, *this, x, y, w, h);
               iter._state._pickMode = true;
               g->accept(iter);
            }
            else {
               R->setFillColor(_defaultColor);
               R->setStrokeColor(_defaultColor);
               Visitor< XRenderer> iter(*R, *this, x, y, w, h);
               g->accept(iter);
            }
         }
         else {
            unique_ptr< SimpleRenderer> R(new SimpleRenderer(_renderContext));
            R->setClipRectangle(R->makePoint(ul), R->makePoint(lr));
            if (_pickInfo.get()) {
               SimpleRenderer::OutPoint pt = R->makePoint(Point(_pickInfo->x, _pickInfo->y));
               unique_ptr< PickRenderer< SimpleRenderer> > P(new PickRenderer< SimpleRenderer>(move(R), pt));
               P->setFillColor(_defaultColor);
               P->setStrokeColor(_defaultColor);
               Visitor< PickRenderer< SimpleRenderer> > iter(*P, *this, x, y, w, h);
               iter._state._pickMode = true;
               g->accept(iter);
            }
            else {
               R->setFillColor(_defaultColor);
               R->setStrokeColor(_defaultColor);
               Visitor< SimpleRenderer> iter(*R, *this, x, y, w, h);
               g->accept(iter);
            }
         }
      }
   }
}
