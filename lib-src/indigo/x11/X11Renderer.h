#ifndef _INDIGO_X11_X11RENDERER_H
#define _INDIGO_X11_X11RENDERER_H

#ifndef _XLIB_H_
#include <X11/Xlib.h>
#endif

#ifndef X_H
#include <X11/X.h>
#endif

#ifndef _INDIGO_NODE_H
#include <indigo/Node.h>
#endif

#ifndef _INDIGO_PICKINFO_H
#include <indigo/PickInfo.h>
#endif

#ifndef _INDIGO_COLOR_H
#include <indigo/Color.h>
#endif

#ifndef _INDIGO_X11_UTIL_H
#include <indigo/x11/util.h>
#endif

#ifndef _TIMBER_LOGGING_H
#include <timber/logging.h>
#endif

namespace indigo {
   namespace x11 {
      class X11Font;
      class X11Image;
      class X11RenderContext;

      /**
       * A graphics renderer that is capable of rendering
       * a 2d scene graph into a drawable. Drawing is accomplished
       * through standard X11 drawing methods. The render is not
       * threadsafe.<p>
       * Currently X11Renderer can use either X11 rendering or XRender based rendering. When using
       * direct X11 rendering, lines are draw very fast, but at the expense of thing like transparency. Use
       * the environment variable {@link INDIGO_X11_XRENDERER} to enable or disable the Render extension.
       * The default enables the use of the Render extension.
       */
      class X11Renderer : public ::timber::Counted
      {
            X11Renderer();
            X11Renderer&operator=(const X11Renderer&);
            X11Renderer(const X11Renderer&);

            /**
             * A pick info object
             */
         public:
            struct PickInfo
            {
                  // the picked pixel
                  size_t x, y;

                  // the info node closest to the picked node
                  ::timber::Pointer< ::indigo::PickInfo> info;
                  // the picked node
                  ::timber::Pointer< ::indigo::Node> picked;
                  // the shape that was picked if the node was composite node or a shapes node
                  size_t detail;
            };

            /** The gfx traversal mechanism */
         private:
            template<class T> class Visitor;
            class XRenderVisitor;

            /**
             * Create a new renderer using the specified drawable.
             * @param rc the rendering context
             * @param defColor the default pixel color
             */
         private:
            X11Renderer(const ::timber::Reference< X11RenderContext>& rc, const ::indigo::Color& defColor)throws();

            /**
             * Create a new renderer using the specified drawable.
             * @param rc the rendering context
             * @param defColor the default pixel color
             */
         public:
            static ::timber::Reference< X11Renderer> create(const ::timber::Reference< X11RenderContext>& rc,
                  const ::indigo::Color& defColor)throws();

            /** Destroy this renderer */
         public:
            virtual ~X11Renderer()throws();

            /**
             * Set the default font.
             * @param font  a default font or 0
             */
         public:
            void setDefaultFont(const ::timber::Pointer< X11Font>& font)throws();

            /**
             * Get the currently set default font.
             * @return the default font
             */
         public:
            ::timber::Pointer< X11Font> defaultFont() const throws();

            /**
             * Destroy this renderer
             */
         public:
            void destroy()throws();

            /**
             * Render the specified scenegraph and clip it to the specified rectangle.
             * @param g a graphic to be rendererd
             * @param x the x-coordinate of the top-left corner
             * @param y the y-coordinate of the top-left corner
             * @param w the width of the clip rectangle
             * @param h the height of the clip rectangle
             */
         public:
            void render(const ::timber::Reference< Node>& g, double x, double y, double w, double h)throws();

            /**
             * Enable picking at the specified pixel.
             * @param x a pixel
             * @param y a pixel
             */
         public:
            void enablePickMode(size_t x, size_t y)throws();

            /**
             * Disable picking. This clears the reference to the picked node.
             * @param x a pixel
             * @param y a pixel
             */
         public:
            ::std::unique_ptr< PickInfo> clearPickMode()throws();

            /** True if this renderer is in pick mode */
         private:
            ::std::unique_ptr< PickInfo> _pickInfo;

            /** The render context */
         private:
            ::timber::Reference< X11RenderContext> _renderContext;

            /** The default pixel color */
         private:
            const ::indigo::Color _defaultColor;

            /** The default font (if set) */
         private:
            ::timber::Pointer< X11Font> _defaultFont;

            /** The Render extension is available if set */
         private:
            bool _renderExtension;

            /** A logger */
         private:
            ::timber::logging::Log _logger;
      };
   }
}

#endif
