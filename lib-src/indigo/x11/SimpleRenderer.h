#ifndef _INDIGO_X11_SIMPLERENDERER_H
#define _INDIGO_X11_SIMPLERENDERER_H

#ifndef _XLIB_H_
#include <X11/Xlib.h>
#endif

#ifndef X_H
#include <X11/X.h>
#endif

#ifndef _XtIntrinsic_h
#include <X11/Intrinsic.h>
#endif

#ifndef _TIMBER_H_
#include <timber/timber.h>
#endif

#ifndef _TIMBER_MEDIA_IMAGEBUFFER_H
#include <timber/media/ImageBuffer.h>
#endif

#ifndef _INDIGO_COLOR_H
#include <indigo/Color.h>
#endif

#ifndef _INDIGO_ANTIALIASINGMODE_H
#include <indigo/AntiAliasingMode.h>
#endif

namespace indigo {
  class Point;
  class Font;
  class AffineTransform2D;

  namespace x11 {
    class X11Font;
    class X11Image;
    class X11RenderContext;
    
   class SimpleRenderer {
     
     /** A pen (used for filling, outlining, and writing */
   private:
     struct Pen {
       Pen(const ::timber::Reference<X11RenderContext>& ctx) throws();
       ~Pen() throws();

       void setAlpha (double a) throws();
       void setColor (const Color& c) throws();
       
       ::Pixel getPixel() const  throws();

     private:
       ::timber::Reference<X11RenderContext> _ctx;
       mutable bool _changed;
       mutable ::Pixel _pixel;
       double _alpha;
       Color _color;
     };

      /** The output point type */
    public:
      typedef ::XPoint OutPoint;
      
      /**
       * Create a new X11Render policy.
       * @param dp the display
       * @param gc the graphic context
       * @param drawable the drawable
       * @param colormap a color map
       */
    public:
      SimpleRenderer (const ::timber::Reference<X11RenderContext>& ctx) throws();

      /** Destructor */
   public:
      ~SimpleRenderer() throws();


      /**
       * Set a clip rectangle.
       * @param ul the upper left coordinate
       * @param lr the lower right coordinate
       */
   public:
      void setClipRectangle (const OutPoint& ul, const OutPoint& lr) throws();

      /** 
       * Set the solid fill color. This replaces any previously set fill pattern
       * or color.
       * @param c a fill color
       */
      void setFillColor (const Color& c) throws();


      /** 
       * Set the stroke color.
       * @param c a color
       */
      void setStrokeColor (const Color& c) throws();

      /** 
       * Set the stroke width.
       * @param w the stroke width
       */
      void setStrokeWidth (double w) throws();
      
      /**
       * Set the alpha value. This is actually ignored if alpha is != 0
       * @param alpha the new alpha value
       */
      void setAlpha (double a) throws();

      /**
       * Set the anti-aliasing mode.
       * @param mode the anti-aliasing mode
       */
      void setAntiAliasingMode(AntiAliasingMode mode)throws();

      /**
       * Offset the specified point.
       * @param pt a point
       * @param offsetX the offset in x
       * @param offsetY the offset in y
       * @return a new point that is offset the specified number of pixels
       */
      OutPoint offsetPoint (const OutPoint& pt, ::timber::Short offsetX, ::timber::Short offsetY) const throws();
      
      /**
       * Make a point.
       * @param pt 
       */
      OutPoint makePoint (const Point& pt) const throws();
      
      /**
       * A make a point.
       * @param in the input point
       * @param out the output point
       */
      void makePoint (const Point& in, OutPoint& out) const throws();
      
      /**
       * Make a point.
       * @param pt 
       */
      OutPoint makePointSafe (const Point& pt) const throws();
      
      /**
       * A make a point.
       * @param in the input point
       * @param out the output point
       */
      void makePointSafe (const Point& in, OutPoint& out) const throws();
      
      /**
       * Fill a polygon. The polygon is assumed to be non-convex.
       * @param pts the polygon points
       * @param n the number of points
       */
      void fillPolygon (OutPoint* pts, size_t n) throws();
      
      /**
       * Draw the outline of a polygon. This can be simulated with drawPolyline
       * to some extent, but the scanline conversion for a polygon is typically
       * different from that of poly line.
       * @param pts the polygon points
       * @param n the number of points
       */
      void drawPolygon (const OutPoint* pts, size_t n) throws();
      
      /**
       * Fill a polygon. The polygon is assumed to be convex.
       * @param pts the polygon points
       * @param n the number of points
       */
      void fillConvexPolygon (const OutPoint* pts, size_t n) throws();
      
      /**
       * Fill a list of triangles. The number of points.
       * @param pts the triangle vertices
       * @param n the number of pointers.
       */
      void fillTriangles (const OutPoint* pts, size_t n) throws();

      /**
       * Fill a list of triangles defined as a triangle strip. The input points 
       * are modified by this call.
       * @param pts the triangle vertices
       * @param n the number of points defining the triangle strip
       */
      void fillTriStrip (OutPoint* pts, size_t n) throws();

      /**
       * Fill a list of triangles defined as a triangle fan. The input points 
       * are modified by this call.
       * @param pts the triangle vertices
       * @param n the number of points defining the triangle fan
       */
      void fillTriFan (OutPoint* pts, size_t n) throws();
	
      /**
       * Draw a filled rectangle defined by its top-left  and lower-right corners. The points
       * are inclusive!
       * @param ul the top-left corner
       * @param lr the lower-right corner
       */
      void fillRectangle (const OutPoint& ul, const OutPoint& lr) throws();

      /**
       * Draw a points (1 pixel wide only)
       * @param pts the polygon points
       * @param n the number of points
       */
      void drawPoints (const OutPoint* pts, size_t n) throws();
	
      /**
       * Draw a polyline (1 pixel wide only)
       * @param pts the polygon points
       * @param n the number of points
       * @param close true if line from the first to the last point should be drawn
       */
      void drawPolyline (const OutPoint* pts, size_t n, bool close=false) throws();
      
      /**
       * Draw rectangle defined by its top-left  and lower-right corners. The points
       * are inclusive!
       * @param ul the top-left corner
       * @param lr the lower-right corner
       */
      void drawRectangle (const OutPoint& ul, const OutPoint& lr) throws();

      /**
       * Render an image. The location of the image is defined by the affine transform.
       * @param image an image
       * @param  an affine transform that as applied to the image.
       */
      void renderImage (const ::timber::media::ImageBuffer& image, const AffineTransform2D& T) throws();

      /**
       * Render an image at the specified location.
       * @param image the image
       * @param T the location of the top-left image corner
       */
      void renderImage (const ::timber::media::ImageBuffer& image, const OutPoint& T) throws();

       /**
       * Set the current font 
       * @param f the new font 
       */
      void setFont (const ::timber::Pointer< ::indigo::Font>& font);

      /**
       * Draw a string with the current font 
       */
      void drawText (const ::std::string& str, const OutPoint& pt);
      

      /**
       * Read the color of the specified pixel.
       * @param pt a pixel 
       * @return a color 
       */
    public:
      Color readPixel (const OutPoint& pt);

      /**
       * Check a specific pixel. This is required by X11Renderer for picking.
       * @return always false
       */
   public:
      inline bool checkPixel() throws() { return false; }

      /** 
       * Activate the specified pen. 
       * @param p the pen to activate
       */
   private:
      void activatePen (const Pen& p) throws();

     /** The pixel color */
    private:
      ::Pixel _pixel;
	
      /** The render context */
   private:
      ::timber::Reference<X11RenderContext> _ctx;

      /** An X11 image */
    private:
      ::std::unique_ptr<X11Image> _image;

      /** The current font */
   private:
      ::timber::Pointer<X11Font> _font;

      /** The pens */
   private:
      Pen _fill,_stroke,_fontColor;

      /** The current alpha value (used for images) */
   private:
      double _alpha;
    };
  }
}

#endif
