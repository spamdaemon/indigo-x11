# this makefile fragment defines the external libraries
# for the libraries, examples, tests, and binaries
# 
# the following definitions must be output from this file:
# EXT_LIB_SEARCH_PATH : the search paths for libraries
# EXT_LINK_LIBS       : the libraries with which to link (in the proper link order)
# EXT_INC_SEARCH_PATH : the search paths for include files
EXT_LIB_SEARCH_PATH :=
EXT_LINK_LIBS       :=
EXT_INC_SEARCH_PATH :=

# Need X11 and optionally GL/GLU
#X11_HOME := /usr/X11
#EXT_LIB_SEARCH_PATH += $(X11_HOME)/lib
#EXT_INC_SEARCH_PATH += $(X11_HOME)/include
EXT_LINK_LIBS += -lXft -lXt -lX11 

# freetype
FREETYPE2_HOME := /usr/include/freetype2
EXT_INC_SEARCH_PATH += $(FREETYPE2_HOME)
EXT_LINK_LIBS += -lfreetype

#GL_HOME := /usr/X11
#EXT_LIB_SEARCH_PATH += $(X11_HOME)/lib
#EXT_INC_SEARCH_PATH += $(X11_HOME)/include
EXT_LINK_LIBS += -lGLU -lGL


# this is an optional definition
# EXT_COMPILER_DEFINES : definitions passed to the compiler (without the -D option)
#EXT_CPP_OPTS := compiler options for c++ compilation
#EXT_C_OPTS := compiler options for c compilation
#EXT_FORTRAN_OPTS := compiler options for fortran compilation

EXT_COMPILER_DEFINES :=
EXT_CPP_OPTS := 
EXT_C_OPTS := 
EXT_FORTRAN_OPTS := 
