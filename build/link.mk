# this fragment defines how to create a shared library

# these are two helper includes
COMMA := ,
EMPTY :=
SPACE := $(empty) $(empty)

define create.so
	g++ -shared -o $@ $^ \
        $(if $(filter true,$(COVERAGE)), $(LINKER_COVERAGE_OPTS)) \
	$(addprefix -Wl$(COMMA)-rpath$(COMMA),$(BASE_LIB_SEARCH_PATH) $(PROJ_LIB_SEARCH_PATH) $(EXT_LIB_SEARCH_PATH))\
	$(addprefix -L,$(PROJ_LIB_SEARCH_PATH)) $(PROJ_LINK_LIBS) \
	$(addprefix -L,$(EXT_LIB_SEARCH_PATH)) $(EXT_LINK_LIBS)
endef

# link an executable
define link.bin
	g++ -o $@ $< \
        $(if $(filter true,$(COVERAGE)), $(LINKER_COVERAGE_OPTS)) \
	$(addprefix -Wl$(COMMA)-rpath$(COMMA),$(BASE_LIB_SEARCH_PATH) $(PROJ_LIB_SEARCH_PATH) $(EXT_LIB_SEARCH_PATH))\
	$(addprefix -L,$(BASE_LIB_SEARCH_PATH)) $(BASE_LINK_LIBS) \
	$(addprefix -L,$(PROJ_LIB_SEARCH_PATH)) $(PROJ_LINK_LIBS) \
	$(addprefix -L,$(EXT_LIB_SEARCH_PATH)) $(EXT_LINK_LIBS)
endef

define link.test 
	g++ -shared -o $@ $< \
        $(if $(filter true,$(COVERAGE)), $(LINKER_COVERAGE_OPTS)) \
	$(addprefix -Wl$(COMMA)-rpath$(COMMA),$(BASE_LIB_SEARCH_PATH) $(PROJ_LIB_SEARCH_PATH) $(EXT_LIB_SEARCH_PATH))\
	$(addprefix -L,$(BASE_LIB_SEARCH_PATH)) $(TEST_LINK_LIBS) $(BASE_LINK_LIBS) \
	$(addprefix -L,$(PROJ_LIB_SEARCH_PATH)) $(PROJ_LINK_LIBS) \
	$(addprefix -L,$(EXT_LIB_SEARCH_PATH)) $(EXT_LINK_LIBS)
endef

# this is actually just the same as create.so
define create.module 
	g++ -shared -o $@ $^ \
        $(if $(filter true,$(COVERAGE)), $(LINKER_COVERAGE_OPTS)) \
	$(addprefix -Wl$(COMMA)-rpath$(COMMA),$(BASE_LIB_SEARCH_PATH) $(PROJ_LIB_SEARCH_PATH) $(EXT_LIB_SEARCH_PATH))\
	$(addprefix -L,$(PROJ_LIB_SEARCH_PATH)) $(PROJ_LINK_LIBS) \
	$(addprefix -L,$(EXT_LIB_SEARCH_PATH)) $(EXT_LINK_LIBS)
endef
