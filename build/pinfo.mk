# This makefile fragment defines information about the project:
# BASE_NAME : The name of the project (usually just inferred from the lib-src directory)
# BASE_SOURCE_DIRS : the directories containing sources for the project
# BASE_INC_SEARCH_PATH : the search path for includes (a subset of the BASE_SOURCE_DIRS)
# BASE_LIB_SEARCH_PATH : the search path for the project libraries
# BASE_LIB : the absolute path to the project library
# BASE_LINK_LIB: the link instruction for the project library
#
# BASE_DIR variables will be defined before running this makefile

# the project name is inferred from the target library directory
BASE_NAME := indigo-x11
BASE_SOURCE_DIRS := test-lib-src gen-lib-src lib-src bin-src tests-src examples-src
BASE_INC_SEARCH_PATH := $(addprefix $(BASE_DIR)/,$(BASE_SOURCE_DIRS))
BASE_LIB_SEARCH_PATH := $(BASE_DIR)/lib

BASE_LINK_LIBS  := -l$(BASE_NAME)
BASE_LIB := $(BASE_LIB_SEARCH_PATH)/lib$(BASE_NAME).so

TEST_LINK_LIBS  := -l$(BASE_NAME)Test
TEST_LIB := $(BASE_LIB_SEARCH_PATH)/lib$(BASE_NAME)Test.so

C_DEFINES := 
CPP_DEFINES := $(C_DEFINES)

